#Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

#SPDX-License-Identifier: Apache-2.0





$ECHO "$GREY[*] all test cases completed."

if [ ${FAILTEST} -eq 0 ];then
    $ECHO "$GREEN[+] all tests were successful :-)"
else
    $ECHO "$RED[!] some tests were fail :-("
fi