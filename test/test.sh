#Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

#SPDX-License-Identifier: Apache-2.0

source ./test_pre.sh
source ./test_all.sh
source ./test_post.sh