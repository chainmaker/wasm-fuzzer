#Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

#SPDX-License-Identifier: Apache-2.0



#case1 正常执行 -i -o -V
$ECHO "$GREY[*] running wasmfuzzer , case1 will take approx 60 seconds" 
../build/submodule/afl/wasmfuzzer-exec -V 60 -i input1 -o output1 ./wasmer @@ -i add 1 1 > out1
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case1 running wasmfuzzer success "
else
    $ECHO "$RED[!] case1 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output1
#case2 正常执行 -V -i -o -T 
$ECHO "$GREY[*] running wasmfuzzer , case2 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -T test_banner -V 60 -i input2 -o output1 ./wasmer @@ -i add 1 1 > out2
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case2 running wasmfuzzer success "
else
    $ECHO "$RED[!] case2 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output1

#case3 正常执行 -V -i -o  
$ECHO "$GREY[*] running wasmfuzzer , case3 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -V 60 -i input2 -o output1 ./wasmer @@ -i add 1 1 > out3
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case3 running wasmfuzzer success "
else
    $ECHO "$RED[!] case3 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output1

#case4 正常执行 -n -i -o
$ECHO "$GREY[*] running wasmfuzzer , case4 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -n -V 60 -i input1 -o output1 ./wasmer @@ -i add 1 1 > out4
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case4 running wasmfuzzer success "
else
    $ECHO "$RED[!] case4 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output1

#case5 正常执行 -E -i -o
$ECHO "$GREY[*] running wasmfuzzer , case5 will take approx 1024 times"
../build/submodule/afl/wasmfuzzer-exec -E 1024 -i input3 -o output2 ./wasmer @@ -i add 1 1 > out5
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case5 running wasmfuzzer success "
else
    $ECHO "$RED[!] case5 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case6 正常执行 -E -i -o
$ECHO "$GREY[*] running wasmfuzzer , case6 will take approx 0 times"
../build/submodule/afl/wasmfuzzer-exec -E 0 -i input3 -o output2 ./wasmer @@ -i add 1 1 > out6
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case6 running wasmfuzzer success "
else
    $ECHO "$RED[!] case6 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case7 正常执行 -E -i -o 
$ECHO "$GREY[*] running wasmfuzzer , case7 will take approx 65535 times"
../build/submodule/afl/wasmfuzzer-exec -E 65535 -i input3 -o output2 ./wasmer @@ -i add 1 1 > out7
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case7 running wasmfuzzer success "
else
    $ECHO "$RED[!] case7 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case8 正常执行 -b -i -o -V 
$ECHO "$GREY[*] running wasmfuzzer , case8 will take approx 300 seconds"
../build/submodule/afl/wasmfuzzer-exec -b 0 -V 300 -i input3 -o output2 ./wasmer @@ -i add 1 1 > out8
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case8 running wasmfuzzer success "
else
    $ECHO "$RED[!] case8 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case9 正常执行 -b -i -o -V 
$ECHO "$GREY[*] running wasmfuzzer , case9 will take approx 300 seconds"
../build/submodule/afl/wasmfuzzer-exec -b 1 -V 60 -i input3 -o output2 ./wasmer @@ -i add 1 1 > out9
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case9 running wasmfuzzer success "
else
    $ECHO "$RED[!] case9 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case10 正常执行 -e -i -o -V 
$ECHO "$GREY[*] running wasmfuzzer , case10 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -e wasm -V 60 -i input1 -o output3 ./wasmer @@ -i add 1 1 > out10
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case10 running wasmfuzzer success "
else
    $ECHO "$RED[!] case10 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output3

#case11 正常执行 -e -i -o -V 
$ECHO "$GREY[*] running wasmfuzzer , case11 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -e wasm -V 60 -i input1 -o output3 ./wasmer @@ -i add 1 1 > out11
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case11 running wasmfuzzer success "
else
    $ECHO "$RED[!] case11 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output3

#case12 正常执行 -m -i -o -V
$ECHO "$GREY[*] running wasmfuzzer , case12 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -m 1024 -V 60 -i input1 -o output3 ./wasmer @@ -i add 1 1 > out12
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case12 running wasmfuzzer success "
else
    $ECHO "$RED[!] case12 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output3

#case13 正常执行 -m -i -o -V
$ECHO "$GREY[*] running wasmfuzzer , case13 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -m 512 -V 60 -i input1 -o output3 ./wasmer @@ -i add 1 1 > out13
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case13 running wasmfuzzer success "
else
    $ECHO "$RED[!] case13 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output3

#case14 正常执行 -m -i -o -V
$ECHO "$GREY[*] running wasmfuzzer , case14 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -m 2048 -V 60 -i input2 -o output2 ./wasmer @@ -i add 1 1 > out14
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case14 running wasmfuzzer success "
else
    $ECHO "$RED[!] case14 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case15 正常执行 -t -i -o -V
$ECHO "$GREY[*] running wasmfuzzer , case15 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -t 1000 -V 60 -i input2 -o output2 ./wasmer @@ -i add 1 1 > out15
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case15 running wasmfuzzer success "
else
    $ECHO "$RED[!] case15 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case16 正常执行 -t -i -o -V
$ECHO "$GREY[*] running wasmfuzzer , case16 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -t 65536 -V 60 -i input3 -o output2 ./wasmer @@ -i add 1 1 > out16
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case16 running wasmfuzzer success "
else
    $ECHO "$RED[!] case16 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case17 正常执行 -t -i -o -V
$ECHO "$GREY[*] running wasmfuzzer , case17 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -t 10 -V 60 -i input2 -o output2 ./wasmer @@ -i add 1 1 > out17
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case17 running wasmfuzzer success "
else
    $ECHO "$RED[!] case17 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case18 正常执行 -m -t -i -o -V
$ECHO "$GREY[*] running wasmfuzzer , case18 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -t 10000 -m none -V 60 -i input2 -o output2 ./wasmer @@ -i add 1 1 > out18
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case18 running wasmfuzzer success "
else
    $ECHO "$RED[!] case18 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case19 正常执行 -b -m -t -i -o -V -T 
$ECHO "$GREY[*] running wasmfuzzer , case19 will take approx 60 seconds"
../build/submodule/afl/wasmfuzzer-exec -b 1 -T test_banner -t 10000 -m none -V 60 -i input2 -o output2 ./wasmer @@ -i add 1 1 > out19
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case19 running wasmfuzzer success "
else
    $ECHO "$RED[!] case19 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case20 正常执行 -b -m -t -i -o -E -T 
$ECHO "$GREY[*] running wasmfuzzer , case20 will take approx 10000 times"
../build/submodule/afl/wasmfuzzer-exec -b 1 -T test_banner -t 10000 -m none -E 10000 -i input2 -o output2 ./wasmer @@ -i add 1 1 > out20
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case20 running wasmfuzzer success "
else
    $ECHO "$RED[!] case20 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output2

#case21 正常执行 -b -m -t -i -o -V -T 
$ECHO "$GREY[*] running wasmfuzzer , case21 will take approx 600 seconds"
../build/submodule/afl/wasmfuzzer-exec -b 1 -t 10000 -m none -V 600 -i input1 -o output3 ./wasmer @@ -i add 1 1 > out21
if [ $? -eq 0 ];then
    $ECHO "$GREEN[+] case21 running wasmfuzzer success "
else
    $ECHO "$RED[!] case21 running wasmfuzzer fail "
    FAILTEST=1
fi
rm -rf output3

#case22 测试crash1 
$ECHO "$GREY[*] running wasmfuzzer ,crash1 will make wasmer crash"
./wasmer crash1  > out22
if [ $? -ne 0 ];then
    $ECHO "$GREEN[+] case22 running wasmfuzzer success "
else
    $ECHO "$RED[!] case22 running wasmfuzzer fail "
    FAILTEST=1
fi

#case23 测试crash2
$ECHO "$GREY[*] running wasmfuzzer ,crash2 will make wasmer crash"
./wasmer crash2  > out23
if [ $? -ne 0 ];then
    $ECHO "$GREEN[+] case23 running wasmfuzzer success "
else
    $ECHO "$RED[!] case23 running wasmfuzzer fail "
    FAILTEST=1
fi