/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/

pipeline {
    agent any
    environment {
        GOROOT="/usr/local/go"
        GOPATH="/root/go"
        GOPROXY="https://goproxy.io,direct"
        PATH="$PATH:$GOROOT/bin:$GOPATH/bin"
        GIT_BRACH_NAME= "${gitlabSourceBranch}"
    }
    stages {
        stage("prepare") {
            steps {
                git([branch:env.GIT_BRACH_NAME, credentialsId: '81fb262a-634b-4c62-8d88-fb704ea75b56',url:'https://git.code.tencent.com/IBPC/03-smart-contract-verify.git'])
                script {
                    env.GIT_COMMIT = sh (script: 'git rev-parse HEAD', returnStdout: true).trim()
                    env.GIT_EMAIL = sh (script: 'git log -1 --pretty=format:"%ce"', returnStdout: true).trim()
                    env.GIT_COMMITTER = sh (script: 'git log -1 --pretty=format:"%cn %ce"', returnStdout: true).trim()
                }
            }
        }
        stage('Update Status') {
            steps{
                sh '''
                    curl https://git.code.tencent.com/api/v3/projects/IBPC%2F03-smart-contract-verify/commit/${GIT_COMMIT}/statuses -v -X POST --header "PRIVATE-TOKEN: 3DWG3qkb7cZzWvZqmjwp" --header "Content-Type: application/json" -d '{"state": "pending","target_url": "http://39.96.193.42:8086/job/wasmfuzzer/'${BUILD_NUMBER}'/console","description": "building","context": "jenkins","block": false}'
                '''
            }
        }
        stage("build") {
            steps {
            sh  ''' 
                if test -d ./build ; then
                    rm -r ./build
                fi

                mkdir ./build
                cd build 
                cmake ..
                cmake --build .
                '''
            }
        }
        stage("test") {
            parallel{
                stage('Unit Test'){
                    steps {
                        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                            sh '''
                                sh unitTest.sh
                                
                                coverage_afl=$(genhtml afl.info -o afl_result| tail -2 |head -1 | grep -P "\\d+\\.\\d+(?=\\%)" -o)
                                coverage_afl1=$(genhtml afl1.info -o afl1_result| tail -2 |head -1 | grep -P "\\d+\\.\\d+(?=\\%)" -o)
                                coverage_wasmfuzzer=$(genhtml wasmfuzzer.info -o wasmfuzzer_result| tail -2 |head -1 | grep -P "\\d+\\.\\d+(?=\\%)" -o)
                                coverage_wabt=$(genhtml wabt.info -o wabt_result| tail -2 |head -1 | grep -P "\\d+\\.\\d+(?=\\%)" -o)

                                echo "afl++单测覆盖率: $coverage_afl%" 
                                echo "afl++1单测覆盖率: $coverage_afl1%" 
                                echo "wasmfuzzer单测覆盖率: $coverage_wasmfuzzer%" 
                                echo "wabt单测覆盖率: $coverage_wabt%" 

                                threshold=40
                                [ $(awk 'BEGIN {print ("'${coverage_afl}'" >= "'${threshold}'")}') -eq 1 ]|| (echo "afl单测覆盖率: $coverage_afl% 低于$threshold%"; exit 1)
                                [ $(awk 'BEGIN {print ("'${coverage_afl1}'" >= "'${threshold}'")}') -eq 1 ]|| (echo "afl1单测覆盖率: $coverage_afl1% 低于$threshold%"; exit 1)
                                [ $(awk 'BEGIN {print ("'${coverage_wasmfuzzer}'" >= "'${threshold}'")}') -eq 1 ]|| (echo "wasmfuzzer单测覆盖率: $coverage_wasmfuzzer% 低于$threshold%"; exit 1)
                                [ $(awk 'BEGIN {print ("'${coverage_wabt}'" >= "'${threshold}'")}') -eq 1 ]|| (echo "wabt单测覆盖率: $coverage_wabt% 低于$threshold%"; exit 1)

                                coverage_comment_Cplus=$(/root/gocloc/cmd/gocloc/gocloc --include-lang=C++ --output-type=json . | tail -n +2 |jq '(.total.comment)/(.total.code+.total.comment)*100'| grep -P "\\d+\\.\\d" -o)
                                coverage_comment_C=$(/root/gocloc/cmd/gocloc/gocloc --include-lang=C --output-type=json ./submodule/afl | tail -n +2 |jq '(.total.comment)/(.total.code+.total.comment)*100'| grep -P "\\d+\\.\\d" -o)

                                echo "C++注释覆盖率: $coverage_comment_Cplus%" 
                                echo "C注释覆盖率: $coverage_comment_C%" 
                                [ $(awk "BEGIN {print (${coverage_comment_Cplus} >= 15)}") -eq 1 ] || (echo "C++注释覆盖率: ${coverage_comment_Cplus} 低于 15%"; exit 1)
                                [ $(awk "BEGIN {print (${coverage_comment_C} >= 15)}") -eq 1 ] || (echo "C注释覆盖率: ${coverage_comment_C} 低于 15%"; exit 1)
                               '''
                        }
                        script {
                            if (currentBuild.result=='FAILURE') {  env.BUILD_STATUS = "failed" }
                        }
                    }
                }
                stage('Func Test'){
                    steps {
                        catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                            sh '''
                                cd ./test
                                bash ./test.sh
                               '''
                        }
                        script {
                            if (currentBuild.result=='FAILURE') {  env.BUILD_STATUS = "failed" }
                        }
                    }
                }
            }
        }
    }
    
    post {
        aborted {
            sh '''
                    curl https://git.code.tencent.com/api/v3/projects/IBPC%2F03-smart-contract-verify/commit/${GIT_COMMIT}/statuses -v -X POST --header "PRIVATE-TOKEN: 3DWG3qkb7cZzWvZqmjwp" --header "Content-Type: application/json" -d '{"state": "failure","target_url": "http://39.96.193.42:8086/job/wasmfuzzer/'${BUILD_NUMBER}'/console","description": "build abort","context": "jenkins","block": false}'
                '''
        }
        failure {
            sh '''
                    curl https://git.code.tencent.com/api/v3/projects/IBPC%2F03-smart-contract-verify/commit/${GIT_COMMIT}/statuses -v -X POST --header "PRIVATE-TOKEN: 3DWG3qkb7cZzWvZqmjwp" --header "Content-Type: application/json" -d '{"state": "error","target_url": "http://39.96.193.42:8086/job/wasmfuzzer/'${BUILD_NUMBER}'/console","description": "build error","context": "jenkins","block": false}'
                '''
                emailext body: "本次提交CI构建失败。 请点击以下链接查看错误详情：${BUILD_URL}console",
                subject: "CI构建${JOB_NAME}失败",
                to: "${GIT_EMAIL}"
        }
        success {
            sh '''
                    curl https://git.code.tencent.com/api/v3/projects/IBPC%2F03-smart-contract-verify/commit/${GIT_COMMIT}/statuses -v -X POST --header "PRIVATE-TOKEN: 3DWG3qkb7cZzWvZqmjwp" --header "Content-Type: application/json" -d '{"state": "success","target_url": "http://39.96.193.42:8086/job/wasmfuzzer/'${BUILD_NUMBER}'/console","description": "build success","context": "jenkins","block": false}'
                '''
        }
    }
}
