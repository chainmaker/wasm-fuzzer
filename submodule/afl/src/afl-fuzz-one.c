/*
   american fuzzy lop++ - fuzze_one routines in different flavours
   ---------------------------------------------------------------

   Originally written by Michal Zalewski

   Now maintained by Marc Heuse <mh@mh-sec.de>,
                        Heiko Eißfeldt <heiko.eissfeldt@hexco.de> and
                        Andrea Fioraldi <andreafioraldi@gmail.com>

   Copyright 2016, 2017 Google Inc. All rights reserved.
   Copyright 2019-2022 AFLplusplus Project. All rights reserved.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at:

     https://www.apache.org/licenses/LICENSE-2.0

   This is the real deal: the program takes an instrumented binary and
   attempts a variety of basic fuzzing tricks, paying close attention to
   how they affect the execution path.

 */

#include "afl-fuzz.h"
#include <string.h>
#include <limits.h>
#include "cmplog.h"
#include "wasmfuzzer_lib.h"
#include<time.h>

typedef void (*MutationFunction) (void**);
extern char* stageShortAll[] ;







/* Helper to choose random block len for block operations in fuzz_one().
   Doesn't return zero, provided that max_len is > 0. */

static inline u32 choose_block_len(afl_state_t *afl, u32 limit) {

  u32 min_value, max_value;
  u32 rlim = MIN(afl->queue_cycle, (u32)3);

  if (unlikely(!afl->run_over10m)) { rlim = 1; }

  switch (rand_below(afl, rlim)) {

    case 0:
      min_value = 1;
      max_value = HAVOC_BLK_SMALL;
      break;

    case 1:
      min_value = HAVOC_BLK_SMALL;
      max_value = HAVOC_BLK_MEDIUM;
      break;

    default:

      if (likely(rand_below(afl, 10))) {

        min_value = HAVOC_BLK_MEDIUM;
        max_value = HAVOC_BLK_LARGE;

      } else {

        min_value = HAVOC_BLK_LARGE;
        max_value = HAVOC_BLK_XL;

      }

  }

  if (min_value >= limit) { min_value = 1; }

  return min_value + rand_below(afl, MIN(max_value, limit) - min_value + 1);

}

/* Helper function to see if a particular change (xor_val = old ^ new) could
   be a product of deterministic bit flips with the lengths and stepovers
   attempted by afl-fuzz. This is used to avoid dupes in some of the
   deterministic fuzzing operations that follow bit flips. We also
   return 1 if xor_val is zero, which implies that the old and attempted new
   values are identical and the exec would be a waste of time. */

static u8 could_be_bitflip(u32 xor_val) {

  u32 sh = 0;

  if (!xor_val) { return 1; }

  /* Shift left until first bit set. */

  while (!(xor_val & 1)) {

    ++sh;
    xor_val >>= 1;

  }

  /* 1-, 2-, and 4-bit patterns are OK anywhere. */

  if (xor_val == 1 || xor_val == 3 || xor_val == 15) { return 1; }

  /* 8-, 16-, and 32-bit patterns are OK only if shift factor is
     divisible by 8, since that's the stepover for these ops. */

  if (sh & 7) { return 0; }

  if (xor_val == 0xff || xor_val == 0xffff || xor_val == 0xffffffff) {

    return 1;

  }

  return 0;

}

/* Helper function to see if a particular value is reachable through
   arithmetic operations. Used for similar purposes. */

static u8 could_be_arith(u32 old_val, u32 new_val, u8 blen) {

  u32 i, ov = 0, nv = 0, diffs = 0;

  if (old_val == new_val) { return 1; }

  /* See if one-byte adjustments to any byte could produce this result. */

  for (i = 0; (u8)i < blen; ++i) {

    u8 a = old_val >> (8 * i), b = new_val >> (8 * i);

    if (a != b) {

      ++diffs;
      ov = a;
      nv = b;

    }

  }

  /* If only one byte differs and the values are within range, return 1. */

  if (diffs == 1) {

    if ((u8)(ov - nv) <= ARITH_MAX || (u8)(nv - ov) <= ARITH_MAX) { return 1; }

  }

  if (blen == 1) { return 0; }

  /* See if two-byte adjustments to any byte would produce this result. */

  diffs = 0;

  for (i = 0; (u8)i < blen / 2; ++i) {

    u16 a = old_val >> (16 * i), b = new_val >> (16 * i);

    if (a != b) {

      ++diffs;
      ov = a;
      nv = b;

    }

  }

  /* If only one word differs and the values are within range, return 1. */

  if (diffs == 1) {

    if ((u16)(ov - nv) <= ARITH_MAX || (u16)(nv - ov) <= ARITH_MAX) {

      return 1;

    }

    ov = SWAP16(ov);
    nv = SWAP16(nv);

    if ((u16)(ov - nv) <= ARITH_MAX || (u16)(nv - ov) <= ARITH_MAX) {

      return 1;

    }

  }

  /* Finally, let's do the same thing for dwords. */

  if (blen == 4) {

    if ((u32)(old_val - new_val) <= ARITH_MAX ||
        (u32)(new_val - old_val) <= ARITH_MAX) {

      return 1;

    }

    new_val = SWAP32(new_val);
    old_val = SWAP32(old_val);

    if ((u32)(old_val - new_val) <= ARITH_MAX ||
        (u32)(new_val - old_val) <= ARITH_MAX) {

      return 1;

    }

  }

  return 0;

}

/* Last but not least, a similar helper to see if insertion of an
   interesting integer is redundant given the insertions done for
   shorter blen. The last param (check_le) is set if the caller
   already executed LE insertion for current blen and wants to see
   if BE variant passed in new_val is unique. */

static u8 could_be_interest(u32 old_val, u32 new_val, u8 blen, u8 check_le) {

  u32 i, j;

  if (old_val == new_val) { return 1; }

  /* See if one-byte insertions from interesting_8 over old_val could
     produce new_val. */

  for (i = 0; i < blen; ++i) {

    for (j = 0; j < sizeof(interesting_8); ++j) {

      u32 tval =
          (old_val & ~(0xff << (i * 8))) | (((u8)interesting_8[j]) << (i * 8));

      if (new_val == tval) { return 1; }

    }

  }

  /* Bail out unless we're also asked to examine two-byte LE insertions
     as a preparation for BE attempts. */

  if (blen == 2 && !check_le) { return 0; }

  /* See if two-byte insertions over old_val could give us new_val. */

  for (i = 0; (u8)i < blen - 1; ++i) {

    for (j = 0; j < sizeof(interesting_16) / 2; ++j) {

      u32 tval = (old_val & ~(0xffff << (i * 8))) |
                 (((u16)interesting_16[j]) << (i * 8));

      if (new_val == tval) { return 1; }

      /* Continue here only if blen > 2. */

      if (blen > 2) {

        tval = (old_val & ~(0xffff << (i * 8))) |
               (SWAP16(interesting_16[j]) << (i * 8));

        if (new_val == tval) { return 1; }

      }

    }

  }

  if (blen == 4 && check_le) {

    /* See if four-byte insertions could produce the same result
       (LE only). */

    for (j = 0; j < sizeof(interesting_32) / 4; ++j) {

      if (new_val == (u32)interesting_32[j]) { return 1; }

    }

  }

  return 0;

}

#ifndef IGNORE_FINDS

/* Helper function to compare buffers; returns first and last differing offset.
   We use this to find reasonable locations for splicing two files. */

static void locate_diffs(u8 *ptr1, u8 *ptr2, u32 len, s32 *first, s32 *last) {

  s32 f_loc = -1;
  s32 l_loc = -1;
  u32 pos;

  for (pos = 0; pos < len; ++pos) {

    if (*(ptr1++) != *(ptr2++)) {

      if (f_loc == -1) { f_loc = pos; }
      l_loc = pos;

    }

  }

  *first = f_loc;
  *last = l_loc;

  return;

}

#endif                                                     /* !IGNORE_FINDS */

/* Take the current entry from the queue, fuzz it for a while. This
   function is a tad too long... returns 0 if fuzzed successfully, 1 if
   skipped or bailed out. */

u8 fuzz_one_original(afl_state_t *afl) {

  u32 len, temp_len;
  u32 j;
  u32 i;
  u8 *in_buf, *out_buf, *orig_in, *ex_tmp, *eff_map = 0;
  out_buf=NULL;
  in_buf=NULL;
  u64 havoc_queued = 0, orig_hit_cnt, new_hit_cnt = 0, prev_cksum;
  u32 splice_cycle = 0, perf_score = 100, orig_perf, eff_cnt = 1;

  u8 ret_val = 1, doing_det = 0;

  u8  a_collect[MAX_AUTO_EXTRA];
  u32 a_len = 0;

#ifdef IGNORE_FINDS

  /* In IGNORE_FINDS mode, skip any entries that weren't in the
     initial data set. */

  if (afl->queue_cur->depth > 1) return 1;

#else

  if (unlikely(afl->custom_mutators_count)) {

    /* The custom mutator will decide to skip this test case or not. */

    LIST_FOREACH(&afl->custom_mutator_list, struct custom_mutator, {

      if (el->afl_custom_queue_get &&
          !el->afl_custom_queue_get(el->data, afl->queue_cur->fname)) {

        return 1;

      }

    });

  }

  if (likely(afl->pending_favored)) {

    /* If we have any favored, non-fuzzed new arrivals in the queue,
       possibly skip to them at the expense of already-fuzzed or non-favored
       cases. */ //如果favored或者没有被fuzz过，跳过不感兴趣的

    if ((afl->queue_cur->fuzz_level || !afl->queue_cur->favored) &&
        likely(rand_below(afl, 100) < SKIP_TO_NEW_PROB)) {

      return 1;

    }

  } else if (!afl->non_instrumented_mode && !afl->queue_cur->favored &&

             afl->queued_items > 10) {

    /* Otherwise, still possibly skip non-favored cases, albeit less often.
       The odds of skipping stuff are higher for already-fuzzed inputs and
       lower for never-fuzzed entries. */

    if (afl->queue_cycle > 1 && !afl->queue_cur->fuzz_level) {

      if (likely(rand_below(afl, 100) < SKIP_NFAV_NEW_PROB)) { return 1; }

    } else {

      if (likely(rand_below(afl, 100) < SKIP_NFAV_OLD_PROB)) { return 1; }

    }

  }

#endif                                                     /* ^IGNORE_FINDS */

  if (unlikely(afl->not_on_tty)) {

    ACTF(
        "Fuzzing test case #%u (%u total, %llu crashes saved, "
        "perf_score=%0.0f, exec_us=%llu, hits=%u, map=%u, ascii=%u)...",
        afl->current_entry, afl->queued_items, afl->saved_crashes,
        afl->queue_cur->perf_score, afl->queue_cur->exec_us,
        //likely(afl->n_fuzz) ? afl->n_fuzz[afl->queue_cur->n_fuzz_entry] : 0,
        afl->queue_cur->bitmap_size, afl->queue_cur->is_ascii);
    fflush(stdout);

  }

  //orig_in = in_buf = queue_testcase_get(afl, afl->queue_cur);
  len = afl->queue_cur->len;
  CBoolean readWasmFileResult = readWasmFile(afl->queue_cur->fname, (void**)&in_buf);
  if(readWasmFileResult) PFATAL("Read '%s' by wasm struct failed", afl->queue_cur->fname);

  orig_in = in_buf;

  out_buf = afl_realloc(AFL_BUF_PARAM(out), len);
  if (unlikely(!out_buf)) { PFATAL("alloc"); }

  afl->subseq_tmouts = 0;

  afl->cur_depth = afl->queue_cur->depth;

  /*******************************************
   * CALIBRATION (only if failed earlier on) *
   *******************************************/

  if (unlikely(afl->queue_cur->cal_failed)) {

    u8 res = FSRV_RUN_TMOUT;

    if (afl->queue_cur->cal_failed < CAL_CHANCES) {

      afl->queue_cur->exec_cksum = 0;

      res =
          calibrate_case(afl, afl->queue_cur, in_buf, afl->queue_cycle - 1, 0);

      if (unlikely(res == FSRV_RUN_ERROR)) {

        FATAL("Unable to execute target application");

      }

    }

    if (unlikely(afl->stop_soon) || res != afl->crash_mode) {

      ++afl->cur_skipped_items;
      goto abandon_entry;

    }

  }



  //memcpy(out_buf, in_buf, len);
  out_buf = in_buf;
  /*********************
   * PERFORMANCE SCORE *
   *********************/
  if (likely(!afl->old_seed_selection))
    orig_perf = perf_score = afl->queue_cur->perf_score;
  else
    afl->queue_cur->perf_score = orig_perf = perf_score =
        calculate_score(afl, afl->queue_cur);



// Modifications Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心).
  doing_det = 1;
    if (!isWasmStruct((void**)&out_buf)) {
      // judge if wasm struct
    goto SKIP_ALL_MUTATIONS;
  }

#ifdef WASMFUZZER_USE_OTHER_STRATEGY
  #ifdef WASMFUZZER_USE_RANDOM_SELECTING_STRATEGY
    #ifdef WASMFUZZER_USE_SIMPLE_STRATEGY
      #error "Cannot use 2 strategies"
    #endif
  #endif
  #ifndef WASMFUZZER_USE_RANDOM_SELECTING_STRATEGY
    #ifndef WASMFUZZER_USE_SIMPLE_STRATEGY
      #error "Cannot use 0 strategies"
    #endif
  #endif
#endif
#ifndef WASMFUZZER_USE_OTHER_STRATEGY
  #ifdef WASMFUZZER_USE_RANDOM_SELECTING_STRATEGY
    #error "Cannot use 2 strategies"
  #endif
  #ifdef WASMFUZZER_USE_SIMPLE_STRATEGY
    #error "Cannot use 2 strategies"
  #endif
#endif

#ifdef WASMFUZZER_USE_OTHER_STRATEGY

  /*
     Mutating with 
     Self-adjusting 
     Strategy
  */

  int mutatingCount = 3;
  while (mutatingCount--) {
    // set random seed 
    srand((int)time(0));
    //random select mutation type
    int mutationSelection = rand() & (afl->mutationFunctionPoolSize - 1);

    // stage name is Mutating
    afl->stage_short = "Mutating";
    afl->stage_max   = 1;
    afl->stage_cur   = 0;
    afl->stage_name  = "Mutating";
    afl->stage_val_type = STAGE_VAL_NONE;
    orig_hit_cnt = afl->queued_items + afl->saved_crashes;
    prev_cksum = afl->queue_cur->exec_cksum;


    u32 originQueuedPaths = afl->queued_items;
    u64 originUniqueCrashes = afl->saved_crashes;

    // use select mutation function to out_buf 
    afl->mutationFunctionPool[mutationSelection]((void**)&out_buf);    
    // fuzz and judge out_buf whether saved
    if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;


    int changeMutationFunctionPoolCount = 0;
    if (afl->queued_items > originQueuedPaths) {
      // If a new path is found，increase the proportion of this MutationFunction in the pool
      changeMutationFunctionPoolCount += 2;
    }

    if (afl->queued_items > originUniqueCrashes) {
      // If a new crashes is found，increase the proportion of this MutationFunction in the pool
      changeMutationFunctionPoolCount += 16;
    }

    for (int i = 0; i < changeMutationFunctionPoolCount; ++i) {
      //Modify the mutation pool based on the previous results
      MutationFunction changeToMutation = afl->mutationFunctionPool[mutationSelection];
      mutationSelection = rand() & (afl->mutationFunctionPoolSize - 1);
      if (mutationSelection < 16) {
        //less 16 not change 
        continue;
      }
      afl->mutationFunctionPool[mutationSelection] = changeToMutation;
    }

    new_hit_cnt = afl->queued_items + afl->saved_crashes;
    //the number of out_buf new find 
    afl->stage_finds[STAGE_MUTATING] += new_hit_cnt - orig_hit_cnt;
    afl->stage_cycles[STAGE_MUTATING] += afl->stage_max;

  }

#endif


#ifdef WASMFUZZER_USE_RANDOM_SELECTING_STRATEGY
  /*
     Mutating with Random Selecting Strategy
  */
  int mutatingCount = 3;
  while (mutatingCount--) {
    
    int mutationSelection = rand() & (afl->mutationFunctionPoolSize - 1);

    afl->stage_short = "Mutating";
    afl->stage_max   = 1;
    afl->stage_cur   = 0;
    afl->stage_name  = "Mutating";

    afl->stage_val_type = STAGE_VAL_NONE;
    orig_hit_cnt = afl->queued_items + afl->saved_crashes;
    prev_cksum = afl->queue_cur->exec_cksum;

    u32 originQueuedPaths = afl->queued_items;
    u64 originUniqueCrashes = afl->saved_crashes;

    afl->mutationFunctionPool[mutationSelection]((void**)&out_buf);
    if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

    new_hit_cnt = afl->queued_items + afl->saved_crashes;

    afl->stage_finds[STAGE_INSERT_INSTRUCTION] += new_hit_cnt - orig_hit_cnt;
    afl->stage_cycles[STAGE_INSERT_INSTRUCTION] += afl->stage_max;
  }
#endif



#ifndef WASMFUZZER_USE_SIMPLE_STRATEGY
  /*
     Mutating with Simple Strategy
  */

  /*********************************************
   * Insert Instruction *
   *********************************************/

  //stage name is "Insert Instruction"
  afl->stage_short = "InsInst";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Insert Instruction";

  afl->stage_val_type = STAGE_VAL_NONE;

  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;

  for (afl->stage_cur = 0; afl->stage_cur < afl->stage_max; ++afl->stage_cur) {
    // insert an  instruction 
    insertInstruction((void**)&out_buf);
    //target program execs the modifyed out_buf
    if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  }

  new_hit_cnt = afl->queued_items + afl->saved_crashes;

  // after insert a instruction ，the number of new results discovered
  afl->stage_finds[STAGE_INSERT_INSTRUCTION] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_INSERT_INSTRUCTION] += afl->stage_max;

  /*********************************************
   * Erase Instruction *
   *********************************************/
  //staget name is "Erase Instruction"
  afl->stage_short = "EraInst";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Erase Instruction";

  afl->stage_val_type = STAGE_VAL_NONE;

  // save origin hit cnt
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;

  for (afl->stage_cur = 0; afl->stage_cur < afl->stage_max; ++afl->stage_cur) {
    //erase an instruction 
    eraseInstruction((void**)&out_buf);
    //target program execs the modifyed out_buf
    if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  }

  new_hit_cnt = afl->queued_items + afl->saved_crashes;

  // after erase an instruction  ，the number of new results discovered
  afl->stage_finds[STAGE_ERASE_INSTRUCTION] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_ERASE_INSTRUCTION] += afl->stage_max;

  /*********************************************
   * Move Instruction *
   *********************************************/
  //stage name is "Move Instruction"
  afl->stage_short = "MovInst";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Move Instruction";

  afl->stage_val_type = STAGE_VAL_NONE;

  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;

  for (afl->stage_cur = 0; afl->stage_cur < afl->stage_max; ++afl->stage_cur) {
    // move an instruction
    moveInstruction((void**)&out_buf);
    //target program execs the modifyed out_buf
    if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  }

  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after move an instruction  ，the number of new results discovered
  afl->stage_finds[STAGE_MOVE_INSTRUCTION] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_MOVE_INSTRUCTION] += afl->stage_max;



  /*********************************************
   * Add Function *
   *********************************************/
  //stage name is "Add Function"
  afl->stage_short = "AddFunc";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Add Function";

  afl->stage_val_type = STAGE_VAL_NONE;
  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // add a function
  addFunction((void**)&out_buf);
  //target program execs the modifyed out_buf  
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after add a function  ，the number of new results discovered
  afl->stage_finds[STAGE_ADD_FUNCTION] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_ADD_FUNCTION] += afl->stage_max;

  /*********************************************
   * Erase Function *
   *********************************************/
  //stage name is "Erase Function"
  afl->stage_short = "EraFunc";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Erase Function";


  afl->stage_val_type = STAGE_VAL_NONE;
  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // erase a function
  eraseFunction((void**)&out_buf);
  //target program execs the modifyed out_buf  
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after erase a function  ，the number of new results discovered
  afl->stage_finds[STAGE_ERASE_FUNCTION] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_ERASE_FUNCTION] += afl->stage_max;


  /*********************************************
   * Swap Function *
   *********************************************/
  //stage name is "Swap Function"
  afl->stage_short = "SwaFunc";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Swap Function";

  afl->stage_val_type = STAGE_VAL_NONE;
  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // swap a function
  swapFunction((void**)&out_buf);
  //target program execs the modifyed out_buf 
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;


  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after swap a function  ，the number of new results discovered
  afl->stage_finds[STAGE_SWAP_FUNCTION] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_SWAP_FUNCTION] += afl->stage_max;


  /*********************************************
   * Add Global *
   *********************************************/
  //stage name is "Add Global"
  afl->stage_short = "AddGlobal";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Add Global";

  afl->stage_val_type = STAGE_VAL_NONE;
  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // add a global
  addGlobal((void**)&out_buf);
  //target program execs the modifyed out_buf 
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after add a global  ，the number of new results discovered
  afl->stage_finds[STAGE_ADD_GLOBAL] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_ADD_GLOBAL] += afl->stage_max;


  /*********************************************
   * Erase Global *
   *********************************************/
  //stage name is "Erase Global"
  afl->stage_short = "EraGlobal";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Erase Global";

  afl->stage_val_type = STAGE_VAL_NONE;

  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // erase a global  
  eraseGlobal((void**)&out_buf);
  //target program execs the modifyed out_buf 
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after erase a global  ，the number of new results discovered
  afl->stage_finds[STAGE_ERASE_GLOBAL] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_ERASE_GLOBAL] += afl->stage_max;

  /*********************************************
   * Swap Global *
   *********************************************/
  //stage name is "Swap Global"
  afl->stage_short = "SwaGlobal";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Swap Global";

  afl->stage_val_type = STAGE_VAL_NONE;

  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // swap a global  
  swapGlobal((void**)&out_buf);
  //target program execs the modifyed out_buf 
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after swap two global  ，the number of new results discovered
  afl->stage_finds[STAGE_SWAP_GLOBAL] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_SWAP_GLOBAL] += afl->stage_max;

  /*********************************************
   * Add Export *
   *********************************************/
  //stage name is "add Export"
  afl->stage_short = "AddExport";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Add Export";

  afl->stage_val_type = STAGE_VAL_NONE;

  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // add a export 
  addExport((void**)&out_buf);
  //target program execs the modifyed out_buf 
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after add export  ，the number of new results discovered
  afl->stage_finds[STAGE_ADD_EXPORT] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_ADD_EXPORT] += afl->stage_max;


  /*********************************************
   * Erase Export *
   *********************************************/
  //stage name is "Erase Export"
  afl->stage_short = "EraExport";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Erase Export";

  afl->stage_val_type = STAGE_VAL_NONE;
  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // erase a export 
  eraseExport((void**)&out_buf);  
  //target program execs the modifyed out_buf 
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after erase export  ，the number of new results discovered
  afl->stage_finds[STAGE_ERASE_EXPORT] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_ERASE_EXPORT] += afl->stage_max;

  /*********************************************
   * Swap Export *
   *********************************************/
  //stage name is "Swap Export"
  afl->stage_short = "SwaExport";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Swap Export";

  afl->stage_val_type = STAGE_VAL_NONE;
  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // swap a export 
  swapExport((void**)&out_buf);  
  //target program execs the modifyed out_buf 
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after swap export  ，the number of new results discovered
  afl->stage_finds[STAGE_SWAP_EXPORT] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_SWAP_EXPORT] += afl->stage_max;

  /*********************************************
   * Add Type *
   *********************************************/
  //stage name is "Add Type"
  afl->stage_short = "AddType";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Add Type";

  afl->stage_val_type = STAGE_VAL_NONE;
  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // add a type
  addType((void**)&out_buf);
  //target program execs the modifyed out_buf 
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after add a type  ，the number of new results discovered
  afl->stage_finds[STAGE_ADD_TYPE] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_ADD_TYPE] += afl->stage_max;


  /*********************************************
   * Add Memory *
   *********************************************/
    //stage name is "Add Memory"
  afl->stage_short = "AddMemory";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Add Memory";
  

  afl->stage_val_type = STAGE_VAL_NONE;
  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // add a Memory
  addMemory((void**)&out_buf);
  //target program execs the modifyed out_buf 
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;


  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after add a Memory  ，the number of new results discovered
  afl->stage_finds[STAGE_ADD_MEM] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_ADD_MEM] += afl->stage_max;

  /*********************************************
   * Set Start *
   *********************************************/
      //stage name is "Set Start"
  afl->stage_short = "SetStart";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Set Start";

  afl->stage_val_type = STAGE_VAL_NONE;
  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // set start function
  setStart((void**)&out_buf);
  //target program execs the modifyed out_buf 
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;


  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after set start function  ，the number of new results discovered
  afl->stage_finds[STAGE_SET_START] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_SET_START] += afl->stage_max;

  /*********************************************
   * Erase Start *
   *********************************************/
  //stage name is "Erase Start"
  afl->stage_short = "EraStart";
  afl->stage_max   = 1;
  afl->stage_cur   = 0;
  afl->stage_name  = "Erase Start";

  afl->stage_val_type = STAGE_VAL_NONE;
  // saved origin hit cnt 
  orig_hit_cnt = afl->queued_items + afl->saved_crashes;

  prev_cksum = afl->queue_cur->exec_cksum;
  // erase start function
  eraseStart((void**)&out_buf);
  //target program execs the modifyed out_buf 
  if (common_fuzz_stuff(afl, out_buf, len)) goto abandon_entry;

  new_hit_cnt = afl->queued_items + afl->saved_crashes;
  // after set erase start function  ，the number of new results discovered
  afl->stage_finds[STAGE_ERASE_START] += new_hit_cnt - orig_hit_cnt;
  afl->stage_cycles[STAGE_ERASE_START] += afl->stage_max;  

#endif



SKIP_ALL_MUTATIONS:
  // nothing here

  ret_val = 0;

/* we are through with this queue entry - for this iteration */
abandon_entry:

  afl->splicing_with = -1;

  /* Update afl->pending_not_fuzzed count if we made it through the calibration
     cycle and have not seen this entry before. */

  if (!afl->stop_soon && !afl->queue_cur->cal_failed &&
      !afl->queue_cur->was_fuzzed && !afl->queue_cur->disabled) {

    --afl->pending_not_fuzzed;
    afl->queue_cur->was_fuzzed = 1;
    afl->reinit_table = 1;
    if (afl->queue_cur->favored) { --afl->pending_favored; }

  }

  ++afl->queue_cur->fuzz_level;
  
  
  //ck_free(in_buf);
  //ck_free(out_buf);
  // free in_buf
  freeDataPtr((void**)&in_buf);
  //freeWasmFile((void**)&in_buf);
  ck_free(in_buf);
  //free(orig_in);

  return ret_val;



}



/* larger change for MOpt implementation: the original fuzz_one was renamed
   to fuzz_one_original. All documentation references to fuzz_one therefore
   mean fuzz_one_original */

u8 fuzz_one(afl_state_t *afl) {

  int key_val_lv_1 = 0, key_val_lv_2 = 0;

#ifdef _AFL_DOCUMENT_MUTATIONS

  u8 path_buf[PATH_MAX];
  if (afl->do_document == 0) {

    snprintf(path_buf, PATH_MAX, "%s/mutations", afl->out_dir);
    afl->do_document = mkdir(path_buf, 0700);  // if it exists we do not care
    afl->do_document = 1;

  } else {

    afl->do_document = 2;
    afl->stop_soon = 2;

  }

#endif

  // if limit_time_sig == -1 then both are run after each other

  //if (afl->limit_time_sig <= 0) { key_val_lv_1 = fuzz_one_original(afl); }

  key_val_lv_1 = fuzz_one_original(afl);

  //这里被注释掉了
  // if (afl->limit_time_sig != 0) {

  //   if (afl->key_module == 0) {

  //     key_val_lv_2 = pilot_fuzzing(afl);

  //   } else if (afl->key_module == 1) {

  //     key_val_lv_2 = core_fuzzing(afl);

  //   } else if (afl->key_module == 2) {

  //     pso_updating(afl);

  //   }

  // }

  //return (key_val_lv_1 | key_val_lv_2);
  return key_val_lv_1;
}

