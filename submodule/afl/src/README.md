# Source Folder

Quick explanation about the files here:


- `afl-common.c`	- common functions, used by afl-analyze, afl-fuzz, afl-showmap and afl-tmin
- `afl-forkserver.c`	- forkserver implementation, used by afl-fuzz afl-showmap, afl-tmin
- `afl-fuzz-bitmap.c`	- afl-fuzz bitmap handling
- `afl-fuzz.c`		- afl-fuzz binary tool (just main() and usage())
 calls
- `afl-fuzz-init.c`	- afl-fuzz initialization
- `afl-fuzz-one.c`      - afl-fuzz fuzzer_one big loop, this is where the mutation is happening
- `afl-fuzz-performance.c`	- hash64 and rand functions
extension
- `afl-fuzz-queue.c`	- afl-fuzz handling the queue
implementation
- `afl-fuzz-run.c`	- afl-fuzz running the target
- `afl-fuzz-state.c`	- afl-fuzz state and globals
- `afl-fuzz-stats.c`	- afl-fuzz writing the statistics file



