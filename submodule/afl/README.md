
# License



03-smart-contract-verify is made available under the Apache License, Version 2.0 (Apache-2.0), located in the [LICENSE](./LICENSE) file.

# American Fuzzy Lop plus plus (AFL++)

## Get Start

The main execution is similar to the AFL

## Instrument wasmer
- We need to modify the build way for wasmer

1. install `afl.rs`

     - We mainly use the library of `afl.rs` to instrument wasmer, so we need to install afl.rs first
    
    ```
    cargo install afl
    ```
    - we need to find `afl-llvm-rt`
    
    path example

    ```
    /home/xue/.local/share/afl.rs/rustc-1.62.0-nightly-e1b71fe/afl.rs-0.12.4/afl-llvm-rt
    ```

2. instrument wasmer

    - Now, we can instrument the wasmer
    ```
    cd wasmer
     
    export RUSTFLAGS="--cfg fuzzing -C debug-assertions -C passes=sancov-module -C codegen-units=1 -C llvm-args=-sanitizer-coverage-level=3 -C llvm-args=-sanitizer-coverage-trace-pc-guard -C llvm-args=-sanitizer-coverage-prune-blocks=0 -C opt-level=3 -C target-cpu=native -C debuginfo=0 -l afl-llvm-rt -L   afl-llvm-rt`-Clink-arg=-fuse-ld=gold"

    export RUSTDOCFLAGS="--cfg fuzzing -C debug-assertions -C passes=sancov-module -C codegen-units=1 -C llvm-args=-sanitizer-coverage-level=3 -C llvm-args=-sanitizer-coverage-trace-pc-guard -C llvm-args=-sanitizer-coverage-prune-blocks=0 -C opt-level=3 -C target-cpu=native -C debuginfo=0 -l afl-llvm-rt -L   afl-llvm-rt`-Clink-arg=-fuse-ld=gold"
    ```
    - `afl-llvm-rt` need to be changed to the path of afl-llvm-rt found in step 
    

    - Then we need to rebuild wasmer
    ```
        cargo build --release --manifest-path lib/cli/Cargo.toml --features cranelift --bin wasmer
    ```
3. Then we can fuzz wasmer


##  Customized for wasmer

- Customized mutation operations for wasm

1. function-based mutation
  
    `insertInstruction` insert an instruction

    `eraseInstruction` delete an instruction

    `moveInstruction` move and instruction

    `addFunction` add an empty function

    `eraseFunction` remove a function

    `swapFunction` Swap the positions of two functions

2. non-function-based mutation

    `addGlobal` add a global variable

    `eraseGlobal` delete a global variable

    `addExport` add an export entry

    `eraseExport` erase an export entry

    `swapExport` swap two export entry

    `addType` add a Type

    `addMemory` increase memory

    `setStart` set start function

    `eraseStart` delete start function

