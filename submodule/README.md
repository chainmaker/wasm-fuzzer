
# License



03-smart-contract-verify is made available under the Apache License, Version 2.0 (Apache-2.0), located in the [LICENSE](./LICENSE) file.

# wasmfuzzer submodule

## wabt 
- WABT is a suite of tools for WebAssembly


## afl 
- AFL is our fuzzing tool. Based on AFL, we add our customizations for wasm virtual machines.
