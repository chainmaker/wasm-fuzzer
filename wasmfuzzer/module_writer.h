/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/

#pragma once

#include "src/binary-writer.h"

namespace wasmfuzzer {

using wabt::MemoryStream;
using wabt::WriteBinaryOptions;
using wabt::Result;
using wabt::Module;
using wabt::string_view;
using wabt::Failed;
using wabt::WriteBinaryModule;

class ModuleWriter {
private:
	static inline MemoryStream writerStream = MemoryStream();
	static inline WriteBinaryOptions writerOptions = WriteBinaryOptions();
public:
	//write file from memory
	Result operator () (const Module& Module, const string_view& fileName);
};

} 
// end of namespace wasmfuzzer