/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/

#pragma once

#include <iostream>
#include <string>
#include <sstream>
#include <random>
#include <limits>

/**
    * @description: Standard random engine.
    */
static std::default_random_engine randomEngine;

/**
    * @description: Initialize the random engine with a certain seed.
    * @param
    *      seed: the seed used for engine
    * @return: -
    */
template <typename Sseq> void initRandomEngine(Sseq& seed) {
    randomEngine.seed(seed);
}

/**
    * @description: Generate a random integer.
    * @param
    *      l: the smallest integer possible
    *      r: the largest integer possible
    * @return: An integer in [l, r].
    */
template <typename T> inline T randomInt(T l, T r) {
    return std::uniform_int_distribution<T>(l, r)(randomEngine);
}

/**
    * @description: Generate a random integer for certain type.
    * @param
    *      type: ONLY USED FOR TEMPLATE, NOT USED IN FUNCTION
    * @return: An integer.
    */
template <typename T, typename std::enable_if<std::is_integral<T>::value, int>::type n = 0> T getRandom(T* type = nullptr) {
    return std::uniform_int_distribution<T>(
        std::numeric_limits<T>::min(),
        std::numeric_limits<T>::max()
        )(randomEngine);
}

/**
    * @description: Generate a random real number for certain type.
    * @param
    *      type: ONLY USED FOR TEMPLATE, NOT USED IN FUNCTION
    * @return: A real number.
    */
template <typename T, typename std::enable_if<std::is_floating_point<T>::value, int>::type n = 0> T getRandom(T* type = nullptr) {
    return (randomInt(0, 1) == 1 ? 1.0 : -1.0) * std::uniform_real_distribution<T>(
        std::numeric_limits<T>::min(),
        std::numeric_limits<T>::max()
        )(randomEngine);
}