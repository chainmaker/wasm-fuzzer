/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/


#pragma once

#include <stdio.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CBoolean uint8_t

typedef enum MutationType {
    NOT_DECIDED,
    NONE,
    OVERWRITE_BYTE
} MutationType;

typedef struct WasmFuzzerFile {
    void* dataPtr;
    //dataLen=-1 means WASM struct
    int32_t dataLen; 
    MutationType mutationType;
    uint64_t mutationPos;
    uint64_t mutationData;
} WasmFuzzerFile;

// read wasm file to memory
CBoolean readWasmFile(char* filename, void** ppModule);
// write wasm file to memory
CBoolean writeWasmFile(char* filename, void** ppModule);
//free ppModule 
CBoolean freeDataPtr(void** ppModule);
//Determine whether it is a wasm file
CBoolean isWasmStruct(void** ppModule);
void libwasmfuzzerTest(void** ppModule);
// insert an Instruction
int insertInstruction(void** ppModule);
// erase an instruction
int eraseInstruction(void** ppModule);
//move an instrction
int moveInstruction(void** ppModule);
//add a function
int addFunction(void** ppModule);
//erase a function
int eraseFunction(void** ppModule);
//swap two functions 
int swapFunction(void** ppModule);
//add a global item
int addGlobal(void** ppModule);
//erase a global item 
int eraseGlobal(void** ppModule);
//swap two global items
int swapGlobal(void** ppModule);
//add a export item
int addExport(void** ppModule);
//erase a export item
int eraseExport(void** ppModule);
// swap two export items
int swapExport(void** ppModule);
// add a type item
int addType(void** ppModule);
// add a memory item
int addMemory(void** ppModule);
// set start 
int setStart(void** ppModule);
//erase start 
int eraseStart(void** ppModule);
//add a table
int addTable(void** ppModule);
// add a data
int addData(void** ppModule);
// add a Import 
int addImport(void** ppModule);

#ifdef __cplusplus
};
#endif