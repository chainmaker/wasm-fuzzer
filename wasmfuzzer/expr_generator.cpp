/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/

#include "expr_generator.h"
#include "src/utf8.h"

namespace wasmfuzzer {

using namespace wabt;

int _PrintError_EXPR_GENERATOR(const char* fmt, ...) {
    return 0;
}

#define PrintError _PrintError_EXPR_GENERATOR

#define ERROR_IF(expr, ...)    \
  do {                         \
    if (expr) {                \
      PrintError(__VA_ARGS__); \
      return Result::Error;    \
    }                          \
  } while (0)

#define ERROR_UNLESS(expr, ...) ERROR_IF(!(expr), __VA_ARGS__)

#define CALLBACK0(member) \
  ERROR_UNLESS(Succeeded(delegate_->member()), #member " callback failed")

#define CALLBACK(member, ...)                             \
  ERROR_UNLESS(Succeeded(delegate_->member(__VA_ARGS__)), \
               #member " callback failed")

// #define CALLBACK0(member) Succeeded(delegate_->member())

// #define CALLBACK(member, ...) Succeeded(delegate_->member(__VA_ARGS__))

// #define CHECK_RESULT(...)

Result ExprGenerator::generateExpr(Opcode opcode, State state)
{
    //generator expression
    state_ = state;
    read_end_ = state.size;
	if (opcode == Opcode::Invalid) {
		opcode = Opcode::FromCode(rand() % Opcode::Invalid);
	}
    switch (opcode) {
    case Opcode::Unreachable:
        CALLBACK0(OnUnreachableExpr);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::Block: {
        Type sig_type;
        CHECK_RESULT(ReadType(&sig_type, "block signature type"));
        ERROR_UNLESS(IsBlockType(sig_type),
            "expected valid block signature type");
        CALLBACK(OnBlockExpr, sig_type);
        CALLBACK(OnOpcodeBlockSig, sig_type);
        break;
    }

    case Opcode::Loop: {
        Type sig_type;
        CHECK_RESULT(ReadType(&sig_type, "loop signature type"));
        ERROR_UNLESS(IsBlockType(sig_type),
            "expected valid block signature type");
        CALLBACK(OnLoopExpr, sig_type);
        CALLBACK(OnOpcodeBlockSig, sig_type);
        break;
    }

    case Opcode::If: {
        Type sig_type;
        CHECK_RESULT(ReadType(&sig_type, "if signature type"));
        ERROR_UNLESS(IsBlockType(sig_type),
            "expected valid block signature type");
        CALLBACK(OnIfExpr, sig_type);
        CALLBACK(OnOpcodeBlockSig, sig_type);
        break;
    }

    case Opcode::Else:
        CALLBACK0(OnElseExpr);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::SelectT: {
        Index count;
        CHECK_RESULT(ReadCount(&count, "num result types"));
        if (count != 1) {
            PrintError("invalid arity in select instrcution: %u", count);
            return Result::Error;
        }
        Type result_type;
        CHECK_RESULT(ReadType(&result_type, "select result type"));
        CALLBACK(OnSelectExpr, result_type);
        CALLBACK0(OnOpcodeBare);
        break;
    }

    case Opcode::Select:
        CALLBACK(OnSelectExpr, Type::Void);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::Br: {
        Index depth;
        CHECK_RESULT(ReadIndex(&depth, "br depth"));
        CALLBACK(OnBrExpr, depth);
        CALLBACK(OnOpcodeIndex, depth);
        break;
    }

    case Opcode::BrIf: {
        Index depth;
        CHECK_RESULT(ReadIndex(&depth, "br_if depth"));
        CALLBACK(OnBrIfExpr, depth);
        CALLBACK(OnOpcodeIndex, depth);
        break;
    }

    case Opcode::BrTable: {
        Index num_targets;
        CHECK_RESULT(ReadCount(&num_targets, "br_table target count"));
        target_depths_.resize(num_targets);

        for (Index i = 0; i < num_targets; ++i) {
            Index target_depth;
            CHECK_RESULT(ReadIndex(&target_depth, "br_table target depth"));
            target_depths_[i] = target_depth;
        }

        Index default_target_depth;
        CHECK_RESULT(
            ReadIndex(&default_target_depth, "br_table default target depth"));

        Index* target_depths = num_targets ? target_depths_.data() : nullptr;

        CALLBACK(OnBrTableExpr, num_targets, target_depths,
            default_target_depth);
        break;
    }

    case Opcode::Return:
        CALLBACK0(OnReturnExpr);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::Nop:
        CALLBACK0(OnNopExpr);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::Drop:
        CALLBACK0(OnDropExpr);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::End:
        if (state_.offset == end_offset) {
            seen_end_opcode = true;
            CALLBACK0(OnEndFunc);
        }
        else {
            CALLBACK0(OnEndExpr);
        }
        break;
        
    case Opcode::I32Const: {
        uint32_t value;
        CHECK_RESULT(ReadS32Leb128(&value, "i32.const value"));
        CALLBACK(OnI32ConstExpr, value);
        CALLBACK(OnOpcodeUint32, value);
        break;
    }
    
    case Opcode::I64Const: {
        uint64_t value;
        CHECK_RESULT(ReadS64Leb128(&value, "i64.const value"));
        CALLBACK(OnI64ConstExpr, value);
        CALLBACK(OnOpcodeUint64, value);
        break;
    }

    case Opcode::F32Const: {
        uint32_t value_bits = 0;
        CHECK_RESULT(ReadF32(&value_bits, "f32.const value"));
        CALLBACK(OnF32ConstExpr, value_bits);
        CALLBACK(OnOpcodeF32, value_bits);
        break;
    }

    case Opcode::F64Const: {
        uint64_t value_bits = 0;
        CHECK_RESULT(ReadF64(&value_bits, "f64.const value"));
        CALLBACK(OnF64ConstExpr, value_bits);
        CALLBACK(OnOpcodeF64, value_bits);
        break;
    }

    case Opcode::V128Const: {
        v128 value_bits;
        ZeroMemory(value_bits);
        CHECK_RESULT(ReadV128(&value_bits, "v128.const value"));
        CALLBACK(OnV128ConstExpr, value_bits);
        CALLBACK(OnOpcodeV128, value_bits);
        break;
    }

    case Opcode::GlobalGet: {
        Index global_index;
        CHECK_RESULT(ReadIndex(&global_index, "global.get global index"));
        CALLBACK(OnGlobalGetExpr, global_index);
        CALLBACK(OnOpcodeIndex, global_index);
        break;
    }

    case Opcode::LocalGet: {
        Index local_index;
        CHECK_RESULT(ReadIndex(&local_index, "local.get local index"));
        CALLBACK(OnLocalGetExpr, local_index);
        CALLBACK(OnOpcodeIndex, local_index);
        break;
    }

    case Opcode::GlobalSet: {
        Index global_index;
        CHECK_RESULT(ReadIndex(&global_index, "global.set global index"));
        CALLBACK(OnGlobalSetExpr, global_index);
        CALLBACK(OnOpcodeIndex, global_index);
        break;
    }

    case Opcode::LocalSet: {
        Index local_index;
        CHECK_RESULT(ReadIndex(&local_index, "local.set local index"));
        CALLBACK(OnLocalSetExpr, local_index);
        CALLBACK(OnOpcodeIndex, local_index);
        break;
    }

    case Opcode::Call: {
        Index func_index;
        CHECK_RESULT(ReadIndex(&func_index, "call function index"));
        CALLBACK(OnCallExpr, func_index);
        CALLBACK(OnOpcodeIndex, func_index);
        break;
    }

    case Opcode::CallIndirect: {
        Index sig_index;
        CHECK_RESULT(ReadIndex(&sig_index, "call_indirect signature index"));
        Index table_index = 0;
        if (options_.features.reference_types_enabled()) {
            CHECK_RESULT(ReadIndex(&table_index, "call_indirect table index"));
        }
        else {
            uint8_t reserved;
            CHECK_RESULT(ReadU8(&reserved, "call_indirect reserved"));
            ERROR_UNLESS(reserved == 0, "call_indirect reserved value must be 0");
        }
        CALLBACK(OnCallIndirectExpr, sig_index, table_index);
        CALLBACK(OnOpcodeUint32Uint32, sig_index, table_index);
        break;
    }

    case Opcode::ReturnCall: {
        Index func_index;
        CHECK_RESULT(ReadIndex(&func_index, "return_call"));
        CALLBACK(OnReturnCallExpr, func_index);
        CALLBACK(OnOpcodeIndex, func_index);
        break;
    }

    case Opcode::ReturnCallIndirect: {
        Index sig_index;
        CHECK_RESULT(ReadIndex(&sig_index, "return_call_indirect"));
        Index table_index = 0;
        if (options_.features.reference_types_enabled()) {
            CHECK_RESULT(
                ReadIndex(&table_index, "return_call_indirect table index"));
        }
        else {
            uint8_t reserved;
            CHECK_RESULT(ReadU8(&reserved, "return_call_indirect reserved"));
            ERROR_UNLESS(reserved == 0,
                "return_call_indirect reserved value must be 0");
        }
        CALLBACK(OnReturnCallIndirectExpr, sig_index, table_index);
        CALLBACK(OnOpcodeUint32Uint32, sig_index, table_index);
        break;
    }

    case Opcode::LocalTee: {
        Index local_index;
        CHECK_RESULT(ReadIndex(&local_index, "local.tee local index"));
        CALLBACK(OnLocalTeeExpr, local_index);
        CALLBACK(OnOpcodeIndex, local_index);
        break;
    }

    case Opcode::I32Load8S:
    case Opcode::I32Load8U:
    case Opcode::I32Load16S:
    case Opcode::I32Load16U:
    case Opcode::I64Load8S:
    case Opcode::I64Load8U:
    case Opcode::I64Load16S:
    case Opcode::I64Load16U:
    case Opcode::I64Load32S:
    case Opcode::I64Load32U:
    case Opcode::I32Load:
    case Opcode::I64Load:
    case Opcode::F32Load:
    case Opcode::F64Load:
    case Opcode::V128Load:
    case Opcode::V128Load8X8S:
    case Opcode::V128Load8X8U:
    case Opcode::V128Load16X4S:
    case Opcode::V128Load16X4U:
    case Opcode::V128Load32X2S:
    case Opcode::V128Load32X2U: {
        Address alignment_log2;
        CHECK_RESULT(ReadAlignment(&alignment_log2, "load alignment"));
        Address offset;
        CHECK_RESULT(ReadAddress(&offset, 0, "load offset"));
        CALLBACK(OnLoadExpr, opcode, alignment_log2, offset);
        CALLBACK(OnOpcodeUint32Uint32, alignment_log2, offset);
        break;
    }

    case Opcode::I32Store8:
    case Opcode::I32Store16:
    case Opcode::I64Store8:
    case Opcode::I64Store16:
    case Opcode::I64Store32:
    case Opcode::I32Store:
    case Opcode::I64Store:
    case Opcode::F32Store:
    case Opcode::F64Store:
    case Opcode::V128Store: {
        Address alignment_log2;
        CHECK_RESULT(ReadAlignment(&alignment_log2, "store alignment"));
        Address offset;
        CHECK_RESULT(ReadAddress(&offset, 0, "store offset"));

        CALLBACK(OnStoreExpr, opcode, alignment_log2, offset);
        CALLBACK(OnOpcodeUint32Uint32, alignment_log2, offset);
        break;
    }

    case Opcode::MemorySize: {
        uint8_t reserved;
        CHECK_RESULT(ReadU8(&reserved, "memory.size reserved"));
        ERROR_UNLESS(reserved == 0, "memory.size reserved value must be 0");
        CALLBACK0(OnMemorySizeExpr);
        CALLBACK(OnOpcodeUint32, reserved);
        break;
    }

    case Opcode::MemoryGrow: {
        uint8_t reserved;
        CHECK_RESULT(ReadU8(&reserved, "memory.grow reserved"));
        ERROR_UNLESS(reserved == 0, "memory.grow reserved value must be 0");
        CALLBACK0(OnMemoryGrowExpr);
        CALLBACK(OnOpcodeUint32, reserved);
        break;
    }

    case Opcode::I32Add:
    case Opcode::I32Sub:
    case Opcode::I32Mul:
    case Opcode::I32DivS:
    case Opcode::I32DivU:
    case Opcode::I32RemS:
    case Opcode::I32RemU:
    case Opcode::I32And:
    case Opcode::I32Or:
    case Opcode::I32Xor:
    case Opcode::I32Shl:
    case Opcode::I32ShrU:
    case Opcode::I32ShrS:
    case Opcode::I32Rotr:
    case Opcode::I32Rotl:
    case Opcode::I64Add:
    case Opcode::I64Sub:
    case Opcode::I64Mul:
    case Opcode::I64DivS:
    case Opcode::I64DivU:
    case Opcode::I64RemS:
    case Opcode::I64RemU:
    case Opcode::I64And:
    case Opcode::I64Or:
    case Opcode::I64Xor:
    case Opcode::I64Shl:
    case Opcode::I64ShrU:
    case Opcode::I64ShrS:
    case Opcode::I64Rotr:
    case Opcode::I64Rotl:
    case Opcode::F32Add:
    case Opcode::F32Sub:
    case Opcode::F32Mul:
    case Opcode::F32Div:
    case Opcode::F32Min:
    case Opcode::F32Max:
    case Opcode::F32Copysign:
    case Opcode::F64Add:
    case Opcode::F64Sub:
    case Opcode::F64Mul:
    case Opcode::F64Div:
    case Opcode::F64Min:
    case Opcode::F64Max:
    case Opcode::F64Copysign:
    case Opcode::I8X16Add:
    case Opcode::I16X8Add:
    case Opcode::I32X4Add:
    case Opcode::I64X2Add:
    case Opcode::I8X16Sub:
    case Opcode::I16X8Sub:
    case Opcode::I32X4Sub:
    case Opcode::I64X2Sub:
    case Opcode::I16X8Mul:
    case Opcode::I32X4Mul:
    case Opcode::I64X2Mul:
    case Opcode::I8X16AddSatS:
    case Opcode::I8X16AddSatU:
    case Opcode::I16X8AddSatS:
    case Opcode::I16X8AddSatU:
    case Opcode::I8X16SubSatS:
    case Opcode::I8X16SubSatU:
    case Opcode::I16X8SubSatS:
    case Opcode::I16X8SubSatU:
    case Opcode::I8X16MinS:
    case Opcode::I16X8MinS:
    case Opcode::I32X4MinS:
    case Opcode::I8X16MinU:
    case Opcode::I16X8MinU:
    case Opcode::I32X4MinU:
    case Opcode::I8X16MaxS:
    case Opcode::I16X8MaxS:
    case Opcode::I32X4MaxS:
    case Opcode::I8X16MaxU:
    case Opcode::I16X8MaxU:
    case Opcode::I32X4MaxU:
    case Opcode::I8X16Shl:
    case Opcode::I16X8Shl:
    case Opcode::I32X4Shl:
    case Opcode::I64X2Shl:
    case Opcode::I8X16ShrS:
    case Opcode::I8X16ShrU:
    case Opcode::I16X8ShrS:
    case Opcode::I16X8ShrU:
    case Opcode::I32X4ShrS:
    case Opcode::I32X4ShrU:
    case Opcode::I64X2ShrS:
    case Opcode::I64X2ShrU:
    case Opcode::V128And:
    case Opcode::V128Or:
    case Opcode::V128Xor:
    case Opcode::F32X4Min:
    case Opcode::F32X4PMin:
    case Opcode::F64X2Min:
    case Opcode::F64X2PMin:
    case Opcode::F32X4Max:
    case Opcode::F32X4PMax:
    case Opcode::F64X2Max:
    case Opcode::F64X2PMax:
    case Opcode::F32X4Add:
    case Opcode::F64X2Add:
    case Opcode::F32X4Sub:
    case Opcode::F64X2Sub:
    case Opcode::F32X4Div:
    case Opcode::F64X2Div:
    case Opcode::F32X4Mul:
    case Opcode::F64X2Mul:
    case Opcode::I8X16Swizzle:
    case Opcode::I8X16NarrowI16X8S:
    case Opcode::I8X16NarrowI16X8U:
    case Opcode::I16X8NarrowI32X4S:
    case Opcode::I16X8NarrowI32X4U:
    case Opcode::V128Andnot:
    case Opcode::I8X16AvgrU:
    case Opcode::I16X8AvgrU:
        CALLBACK(OnBinaryExpr, opcode);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::I32Eq:
    case Opcode::I32Ne:
    case Opcode::I32LtS:
    case Opcode::I32LeS:
    case Opcode::I32LtU:
    case Opcode::I32LeU:
    case Opcode::I32GtS:
    case Opcode::I32GeS:
    case Opcode::I32GtU:
    case Opcode::I32GeU:
    case Opcode::I64Eq:
    case Opcode::I64Ne:
    case Opcode::I64LtS:
    case Opcode::I64LeS:
    case Opcode::I64LtU:
    case Opcode::I64LeU:
    case Opcode::I64GtS:
    case Opcode::I64GeS:
    case Opcode::I64GtU:
    case Opcode::I64GeU:
    case Opcode::F32Eq:
    case Opcode::F32Ne:
    case Opcode::F32Lt:
    case Opcode::F32Le:
    case Opcode::F32Gt:
    case Opcode::F32Ge:
    case Opcode::F64Eq:
    case Opcode::F64Ne:
    case Opcode::F64Lt:
    case Opcode::F64Le:
    case Opcode::F64Gt:
    case Opcode::F64Ge:
    case Opcode::I8X16Eq:
    case Opcode::I16X8Eq:
    case Opcode::I32X4Eq:
    case Opcode::F32X4Eq:
    case Opcode::F64X2Eq:
    case Opcode::I8X16Ne:
    case Opcode::I16X8Ne:
    case Opcode::I32X4Ne:
    case Opcode::F32X4Ne:
    case Opcode::F64X2Ne:
    case Opcode::I8X16LtS:
    case Opcode::I8X16LtU:
    case Opcode::I16X8LtS:
    case Opcode::I16X8LtU:
    case Opcode::I32X4LtS:
    case Opcode::I32X4LtU:
    case Opcode::F32X4Lt:
    case Opcode::F64X2Lt:
    case Opcode::I8X16LeS:
    case Opcode::I8X16LeU:
    case Opcode::I16X8LeS:
    case Opcode::I16X8LeU:
    case Opcode::I32X4LeS:
    case Opcode::I32X4LeU:
    case Opcode::F32X4Le:
    case Opcode::F64X2Le:
    case Opcode::I8X16GtS:
    case Opcode::I8X16GtU:
    case Opcode::I16X8GtS:
    case Opcode::I16X8GtU:
    case Opcode::I32X4GtS:
    case Opcode::I32X4GtU:
    case Opcode::F32X4Gt:
    case Opcode::F64X2Gt:
    case Opcode::I8X16GeS:
    case Opcode::I8X16GeU:
    case Opcode::I16X8GeS:
    case Opcode::I16X8GeU:
    case Opcode::I32X4GeS:
    case Opcode::I32X4GeU:
    case Opcode::F32X4Ge:
    case Opcode::F64X2Ge:
        CALLBACK(OnCompareExpr, opcode);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::I32Clz:
    case Opcode::I32Ctz:
    case Opcode::I32Popcnt:
    case Opcode::I64Clz:
    case Opcode::I64Ctz:
    case Opcode::I64Popcnt:
    case Opcode::F32Abs:
    case Opcode::F32Neg:
    case Opcode::F32Ceil:
    case Opcode::F32Floor:
    case Opcode::F32Trunc:
    case Opcode::F32Nearest:
    case Opcode::F32Sqrt:
    case Opcode::F64Abs:
    case Opcode::F64Neg:
    case Opcode::F64Ceil:
    case Opcode::F64Floor:
    case Opcode::F64Trunc:
    case Opcode::F64Nearest:
    case Opcode::F64Sqrt:
    case Opcode::I8X16Splat:
    case Opcode::I16X8Splat:
    case Opcode::I32X4Splat:
    case Opcode::I64X2Splat:
    case Opcode::F32X4Splat:
    case Opcode::F64X2Splat:
    case Opcode::I8X16Neg:
    case Opcode::I16X8Neg:
    case Opcode::I32X4Neg:
    case Opcode::I64X2Neg:
    case Opcode::V128Not:
    case Opcode::I8X16AnyTrue:
    case Opcode::I16X8AnyTrue:
    case Opcode::I32X4AnyTrue:
    case Opcode::I8X16Bitmask:
    case Opcode::I16X8Bitmask:
    case Opcode::I32X4Bitmask:
    case Opcode::I8X16AllTrue:
    case Opcode::I16X8AllTrue:
    case Opcode::I32X4AllTrue:
    case Opcode::F32X4Ceil:
    case Opcode::F64X2Ceil:
    case Opcode::F32X4Floor:
    case Opcode::F64X2Floor:
    case Opcode::F32X4Trunc:
    case Opcode::F64X2Trunc:
    case Opcode::F32X4Nearest:
    case Opcode::F64X2Nearest:
    case Opcode::F32X4Neg:
    case Opcode::F64X2Neg:
    case Opcode::F32X4Abs:
    case Opcode::F64X2Abs:
    case Opcode::F32X4Sqrt:
    case Opcode::F64X2Sqrt:
    case Opcode::I16X8WidenLowI8X16S:
    case Opcode::I16X8WidenHighI8X16S:
    case Opcode::I16X8WidenLowI8X16U:
    case Opcode::I16X8WidenHighI8X16U:
    case Opcode::I32X4WidenLowI16X8S:
    case Opcode::I32X4WidenHighI16X8S:
    case Opcode::I32X4WidenLowI16X8U:
    case Opcode::I32X4WidenHighI16X8U:
    case Opcode::I8X16Abs:
    case Opcode::I16X8Abs:
    case Opcode::I32X4Abs:
        CALLBACK(OnUnaryExpr, opcode);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::V128BitSelect:
        CALLBACK(OnTernaryExpr, opcode);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::I8X16ExtractLaneS:
    case Opcode::I8X16ExtractLaneU:
    case Opcode::I16X8ExtractLaneS:
    case Opcode::I16X8ExtractLaneU:
    case Opcode::I32X4ExtractLane:
    case Opcode::I64X2ExtractLane:
    case Opcode::F32X4ExtractLane:
    case Opcode::F64X2ExtractLane:
    case Opcode::I8X16ReplaceLane:
    case Opcode::I16X8ReplaceLane:
    case Opcode::I32X4ReplaceLane:
    case Opcode::I64X2ReplaceLane:
    case Opcode::F32X4ReplaceLane:
    case Opcode::F64X2ReplaceLane: {
        uint8_t lane_val;
        CHECK_RESULT(ReadU8(&lane_val, "Lane idx"));
        CALLBACK(OnSimdLaneOpExpr, opcode, lane_val);
        CALLBACK(OnOpcodeUint64, lane_val);
        break;
    }

    case Opcode::I8X16Shuffle: {
        v128 value;
        CHECK_RESULT(ReadV128(&value, "Lane idx [16]"));
        CALLBACK(OnSimdShuffleOpExpr, opcode, value);
        CALLBACK(OnOpcodeV128, value);
        break;
    }

    case Opcode::V128Load8Splat:
    case Opcode::V128Load16Splat:
    case Opcode::V128Load32Splat:
    case Opcode::V128Load64Splat: {
        Address alignment_log2;
        CHECK_RESULT(ReadAlignment(&alignment_log2, "load alignment"));
        Address offset;
        CHECK_RESULT(ReadAddress(&offset, 0, "load offset"));

        CALLBACK(OnLoadSplatExpr, opcode, alignment_log2, offset);
        CALLBACK(OnOpcodeUint32Uint32, alignment_log2, offset);
        break;
    }
    case Opcode::I32TruncF32S:
    case Opcode::I32TruncF64S:
    case Opcode::I32TruncF32U:
    case Opcode::I32TruncF64U:
    case Opcode::I32WrapI64:
    case Opcode::I64TruncF32S:
    case Opcode::I64TruncF64S:
    case Opcode::I64TruncF32U:
    case Opcode::I64TruncF64U:
    case Opcode::I64ExtendI32S:
    case Opcode::I64ExtendI32U:
    case Opcode::F32ConvertI32S:
    case Opcode::F32ConvertI32U:
    case Opcode::F32ConvertI64S:
    case Opcode::F32ConvertI64U:
    case Opcode::F32DemoteF64:
    case Opcode::F32ReinterpretI32:
    case Opcode::F64ConvertI32S:
    case Opcode::F64ConvertI32U:
    case Opcode::F64ConvertI64S:
    case Opcode::F64ConvertI64U:
    case Opcode::F64PromoteF32:
    case Opcode::F64ReinterpretI64:
    case Opcode::I32ReinterpretF32:
    case Opcode::I64ReinterpretF64:
    case Opcode::I32Eqz:
    case Opcode::I64Eqz:
    case Opcode::F32X4ConvertI32X4S:
    case Opcode::F32X4ConvertI32X4U:
    case Opcode::I32X4TruncSatF32X4S:
    case Opcode::I32X4TruncSatF32X4U:
        CALLBACK(OnConvertExpr, opcode);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::Try: {
        Type sig_type;
        CHECK_RESULT(ReadType(&sig_type, "try signature type"));
        ERROR_UNLESS(IsBlockType(sig_type),
            "expected valid block signature type");
        CALLBACK(OnTryExpr, sig_type);
        CALLBACK(OnOpcodeBlockSig, sig_type);
        break;
    }

    case Opcode::Catch: {
        CALLBACK0(OnCatchExpr);
        CALLBACK0(OnOpcodeBare);
        break;
    }

    case Opcode::Rethrow: {
        CALLBACK0(OnRethrowExpr);
        CALLBACK0(OnOpcodeBare);
        break;
    }

    case Opcode::Throw: {
        Index index;
        CHECK_RESULT(ReadIndex(&index, "event index"));
        CALLBACK(OnThrowExpr, index);
        CALLBACK(OnOpcodeIndex, index);
        break;
    }

    case Opcode::BrOnExn: {
        Index depth;
        Index index;
        CHECK_RESULT(ReadIndex(&depth, "br_on_exn depth"));
        CHECK_RESULT(ReadIndex(&index, "event index"));
        CALLBACK(OnBrOnExnExpr, depth, index);
        CALLBACK(OnOpcodeIndexIndex, depth, index);
        break;
    }

    case Opcode::I32Extend8S:
    case Opcode::I32Extend16S:
    case Opcode::I64Extend8S:
    case Opcode::I64Extend16S:
    case Opcode::I64Extend32S:
        CALLBACK(OnUnaryExpr, opcode);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::I32TruncSatF32S:
    case Opcode::I32TruncSatF32U:
    case Opcode::I32TruncSatF64S:
    case Opcode::I32TruncSatF64U:
    case Opcode::I64TruncSatF32S:
    case Opcode::I64TruncSatF32U:
    case Opcode::I64TruncSatF64S:
    case Opcode::I64TruncSatF64U:
        CALLBACK(OnConvertExpr, opcode);
        CALLBACK0(OnOpcodeBare);
        break;

    case Opcode::AtomicNotify: {
        Address alignment_log2;
        CHECK_RESULT(ReadAlignment(&alignment_log2, "load alignment"));
        Address offset;
        CHECK_RESULT(ReadAddress(&offset, 0, "load offset"));

        CALLBACK(OnAtomicNotifyExpr, opcode, alignment_log2, offset);
        CALLBACK(OnOpcodeUint32Uint32, alignment_log2, offset);
        break;
    }

    case Opcode::I32AtomicWait:
    case Opcode::I64AtomicWait: {
        Address alignment_log2;
        CHECK_RESULT(ReadAlignment(&alignment_log2, "load alignment"));
        Address offset;
        CHECK_RESULT(ReadAddress(&offset, 0, "load offset"));

        CALLBACK(OnAtomicWaitExpr, opcode, alignment_log2, offset);
        CALLBACK(OnOpcodeUint32Uint32, alignment_log2, offset);
        break;
    }

    case Opcode::AtomicFence: {
        uint8_t consistency_model;
        CHECK_RESULT(ReadU8(&consistency_model, "consistency model"));
        ERROR_UNLESS(consistency_model == 0,
            "atomic.fence consistency model must be 0");
        CALLBACK(OnAtomicFenceExpr, consistency_model);
        CALLBACK(OnOpcodeUint32, consistency_model);
        break;
    }

    case Opcode::I32AtomicLoad8U:
    case Opcode::I32AtomicLoad16U:
    case Opcode::I64AtomicLoad8U:
    case Opcode::I64AtomicLoad16U:
    case Opcode::I64AtomicLoad32U:
    case Opcode::I32AtomicLoad:
    case Opcode::I64AtomicLoad: {
        Address alignment_log2;
        CHECK_RESULT(ReadAlignment(&alignment_log2, "load alignment"));
        Address offset;
        CHECK_RESULT(ReadAddress(&offset, 0, "load offset"));

        CALLBACK(OnAtomicLoadExpr, opcode, alignment_log2, offset);
        CALLBACK(OnOpcodeUint32Uint32, alignment_log2, offset);
        break;
    }

    case Opcode::I32AtomicStore8:
    case Opcode::I32AtomicStore16:
    case Opcode::I64AtomicStore8:
    case Opcode::I64AtomicStore16:
    case Opcode::I64AtomicStore32:
    case Opcode::I32AtomicStore:
    case Opcode::I64AtomicStore: {
        Address alignment_log2;
        CHECK_RESULT(ReadAlignment(&alignment_log2, "store alignment"));
        Address offset;
        CHECK_RESULT(ReadAddress(&offset, 0, "store offset"));

        CALLBACK(OnAtomicStoreExpr, opcode, alignment_log2, offset);
        CALLBACK(OnOpcodeUint32Uint32, alignment_log2, offset);
        break;
    }

    case Opcode::I32AtomicRmwAdd:
    case Opcode::I64AtomicRmwAdd:
    case Opcode::I32AtomicRmw8AddU:
    case Opcode::I32AtomicRmw16AddU:
    case Opcode::I64AtomicRmw8AddU:
    case Opcode::I64AtomicRmw16AddU:
    case Opcode::I64AtomicRmw32AddU:
    case Opcode::I32AtomicRmwSub:
    case Opcode::I64AtomicRmwSub:
    case Opcode::I32AtomicRmw8SubU:
    case Opcode::I32AtomicRmw16SubU:
    case Opcode::I64AtomicRmw8SubU:
    case Opcode::I64AtomicRmw16SubU:
    case Opcode::I64AtomicRmw32SubU:
    case Opcode::I32AtomicRmwAnd:
    case Opcode::I64AtomicRmwAnd:
    case Opcode::I32AtomicRmw8AndU:
    case Opcode::I32AtomicRmw16AndU:
    case Opcode::I64AtomicRmw8AndU:
    case Opcode::I64AtomicRmw16AndU:
    case Opcode::I64AtomicRmw32AndU:
    case Opcode::I32AtomicRmwOr:
    case Opcode::I64AtomicRmwOr:
    case Opcode::I32AtomicRmw8OrU:
    case Opcode::I32AtomicRmw16OrU:
    case Opcode::I64AtomicRmw8OrU:
    case Opcode::I64AtomicRmw16OrU:
    case Opcode::I64AtomicRmw32OrU:
    case Opcode::I32AtomicRmwXor:
    case Opcode::I64AtomicRmwXor:
    case Opcode::I32AtomicRmw8XorU:
    case Opcode::I32AtomicRmw16XorU:
    case Opcode::I64AtomicRmw8XorU:
    case Opcode::I64AtomicRmw16XorU:
    case Opcode::I64AtomicRmw32XorU:
    case Opcode::I32AtomicRmwXchg:
    case Opcode::I64AtomicRmwXchg:
    case Opcode::I32AtomicRmw8XchgU:
    case Opcode::I32AtomicRmw16XchgU:
    case Opcode::I64AtomicRmw8XchgU:
    case Opcode::I64AtomicRmw16XchgU:
    case Opcode::I64AtomicRmw32XchgU: {
        Address alignment_log2;
        CHECK_RESULT(ReadAlignment(&alignment_log2, "memory alignment"));
        Address offset;
        CHECK_RESULT(ReadAddress(&offset, 0, "memory offset"));

        CALLBACK(OnAtomicRmwExpr, opcode, alignment_log2, offset);
        CALLBACK(OnOpcodeUint32Uint32, alignment_log2, offset);
        break;
    }

    case Opcode::I32AtomicRmwCmpxchg:
    case Opcode::I64AtomicRmwCmpxchg:
    case Opcode::I32AtomicRmw8CmpxchgU:
    case Opcode::I32AtomicRmw16CmpxchgU:
    case Opcode::I64AtomicRmw8CmpxchgU:
    case Opcode::I64AtomicRmw16CmpxchgU:
    case Opcode::I64AtomicRmw32CmpxchgU: {
        Address alignment_log2;
        CHECK_RESULT(ReadAlignment(&alignment_log2, "memory alignment"));
        Address offset;
        CHECK_RESULT(ReadAddress(&offset, 0, "memory offset"));

        CALLBACK(OnAtomicRmwCmpxchgExpr, opcode, alignment_log2, offset);
        CALLBACK(OnOpcodeUint32Uint32, alignment_log2, offset);
        break;
    }

    case Opcode::TableInit: {
        Index segment;
        CHECK_RESULT(ReadIndex(&segment, "elem segment index"));
        Index table_index;
        CHECK_RESULT(ReadIndex(&table_index, "reserved table index"));
        CALLBACK(OnTableInitExpr, segment, table_index);
        CALLBACK(OnOpcodeUint32Uint32, segment, table_index);
        break;
    }

    case Opcode::MemoryInit: {
        Index segment;
        ERROR_IF(data_count_ == kInvalidIndex,
            "memory.init requires data count section");
        CHECK_RESULT(ReadIndex(&segment, "elem segment index"));
        uint8_t reserved;
        CHECK_RESULT(ReadU8(&reserved, "reserved memory index"));
        ERROR_UNLESS(reserved == 0, "reserved value must be 0");
        CALLBACK(OnMemoryInitExpr, segment);
        CALLBACK(OnOpcodeUint32Uint32, segment, reserved);
        break;
    }

    case Opcode::DataDrop:
        ERROR_IF(data_count_ == kInvalidIndex,
            "data.drop requires data count section");
        // Fallthrough.
    case Opcode::ElemDrop: {
        Index segment;
        CHECK_RESULT(ReadIndex(&segment, "segment index"));
        if (opcode == Opcode::DataDrop) {
            CALLBACK(OnDataDropExpr, segment);
        }
        else {
            CALLBACK(OnElemDropExpr, segment);
        }
        CALLBACK(OnOpcodeUint32, segment);
        break;
    }

    case Opcode::MemoryFill: {
        uint8_t reserved;
        CHECK_RESULT(ReadU8(&reserved, "reserved memory index"));
        ERROR_UNLESS(reserved == 0, "reserved value must be 0");
        CALLBACK(OnMemoryFillExpr);
        CALLBACK(OnOpcodeUint32, reserved);
        break;
    }
    case Opcode::MemoryCopy: {
        uint8_t reserved;
        CHECK_RESULT(ReadU8(&reserved, "reserved memory index"));
        ERROR_UNLESS(reserved == 0, "reserved value must be 0");
        CHECK_RESULT(ReadU8(&reserved, "reserved memory index"));
        ERROR_UNLESS(reserved == 0, "reserved value must be 0");
        CALLBACK(OnMemoryCopyExpr);
        CALLBACK(OnOpcodeUint32Uint32, reserved, reserved);
        break;
    }

    case Opcode::TableCopy: {
        Index table_dst;
        Index table_src;
        CHECK_RESULT(ReadIndex(&table_dst, "reserved table index"));
        CHECK_RESULT(ReadIndex(&table_src, "table src"));
        CALLBACK(OnTableCopyExpr, table_dst, table_src);
        CALLBACK(OnOpcodeUint32Uint32, table_dst, table_src);
        break;
    }

    case Opcode::TableGet: {
        Index table;
        CHECK_RESULT(ReadIndex(&table, "table index"));
        CALLBACK(OnTableGetExpr, table);
        CALLBACK(OnOpcodeUint32, table);
        break;
    }

    case Opcode::TableSet: {
        Index table;
        CHECK_RESULT(ReadIndex(&table, "table index"));
        CALLBACK(OnTableSetExpr, table);
        CALLBACK(OnOpcodeUint32, table);
        break;
    }

    case Opcode::TableGrow: {
        Index table;
        CHECK_RESULT(ReadIndex(&table, "table index"));
        CALLBACK(OnTableGrowExpr, table);
        CALLBACK(OnOpcodeUint32, table);
        break;
    }

    case Opcode::TableSize: {
        Index table;
        CHECK_RESULT(ReadIndex(&table, "table index"));
        CALLBACK(OnTableSizeExpr, table);
        CALLBACK(OnOpcodeUint32, table);
        break;
    }

    case Opcode::TableFill: {
        Index table;
        CHECK_RESULT(ReadIndex(&table, "table index"));
        CALLBACK(OnTableFillExpr, table);
        CALLBACK(OnOpcodeUint32, table);
        break;
    }

    case Opcode::RefFunc: {
        Index func;
        CHECK_RESULT(ReadIndex(&func, "func index"));
        CALLBACK(OnRefFuncExpr, func);
        CALLBACK(OnOpcodeUint32, func);
        break;
    }

    case Opcode::RefNull: {
        Type type;
        CHECK_RESULT(ReadRefType(&type, "ref.null type"));
        CALLBACK(OnRefNullExpr, type);
        CALLBACK(OnOpcodeType, type);
        break;
    }

    case Opcode::RefIsNull:
        CALLBACK(OnRefIsNullExpr);
        CALLBACK(OnOpcodeBare);
        break;
        
    default:
        //return ReportUnexpectedOpcode(opcode);
        //WABT_UNREACHABLE;
        break;
    }

    return Result::Ok;
}

template <typename T>
Result ExprGenerator::ReadT(T* out_value,
    const char* type_name,
    const char* desc) {
    if (state_.offset + sizeof(T) > read_end_) {
        PrintError("unable to read %s: %s", type_name, desc);
        return Result::Error;
    }
    memcpy(out_value, state_.data + state_.offset, sizeof(T));
    state_.offset += sizeof(T);
    return Result::Ok;
}

Result ExprGenerator::ReadU8(uint8_t* out_value, const char* desc) {
    return ReadT(out_value, "uint8_t", desc);
}

Result ExprGenerator::ReadU32(uint32_t* out_value, const char* desc) {
    return ReadT(out_value, "uint32_t", desc);
}

Result ExprGenerator::ReadF32(uint32_t* out_value, const char* desc) {
    return ReadT(out_value, "float", desc);
}

Result ExprGenerator::ReadF64(uint64_t* out_value, const char* desc) {
    return ReadT(out_value, "double", desc);
}

Result ExprGenerator::ReadV128(v128* out_value, const char* desc) {
    return ReadT(out_value, "v128", desc);
}

Result ExprGenerator::ReadU32Leb128(uint32_t* out_value, const char* desc) {
    const uint8_t* p = state_.data + state_.offset;
    const uint8_t* end = state_.data + read_end_;
    size_t bytes_read = wabt::ReadU32Leb128(p, end, out_value);
    ERROR_UNLESS(bytes_read > 0, "unable to read u32 leb128: %s", desc);
    state_.offset += bytes_read;
    return Result::Ok;
    //return ReadU32(out_value, desc);
}

Result ExprGenerator::ReadU64Leb128(uint64_t* out_value, const char* desc) {
    const uint8_t* p = state_.data + state_.offset;
    const uint8_t* end = state_.data + read_end_;
    size_t bytes_read = wabt::ReadU64Leb128(p, end, out_value);
    ERROR_UNLESS(bytes_read > 0, "unable to read u64 leb128: %s", desc);
    state_.offset += bytes_read;
    return Result::Ok;
    //return ReadU64(out_value, desc);
}

Result ExprGenerator::ReadS32Leb128(uint32_t* out_value, const char* desc) {
    const uint8_t* p = state_.data + state_.offset;
    const uint8_t* end = state_.data + read_end_;
    size_t bytes_read = wabt::ReadS32Leb128(p, end, out_value);
    ERROR_UNLESS(bytes_read > 0, "unable to read i32 leb128: %s", desc);
    state_.offset += bytes_read;
    return Result::Ok;
    //return ReadU32(out_value, desc);
}

Result ExprGenerator::ReadS64Leb128(uint64_t* out_value, const char* desc) {
    const uint8_t* p = state_.data + state_.offset;
    const uint8_t* end = state_.data + read_end_;
    size_t bytes_read = wabt::ReadS64Leb128(p, end, out_value);
    ERROR_UNLESS(bytes_read > 0, "unable to read i64 leb128: %s", desc);
    state_.offset += bytes_read;
    return Result::Ok;
    //return ReadU64(out_value, desc);
}

/*Result ExprGenerator::ReadU64(uint64_t* out_value, const char* desc) {
    return ReadT(out_value, "uint64_t", desc);
}*/

Result ExprGenerator::ReadType(Type* out_value, const char* desc) {
    uint32_t type = 0;
    CHECK_RESULT(ReadS32Leb128(&type, desc));
    *out_value = static_cast<Type>(type);
    return Result::Ok;
}

Result ExprGenerator::ReadRefType(Type* out_value, const char* desc) {
    uint32_t type = 0;
    CHECK_RESULT(ReadS32Leb128(&type, desc));
    *out_value = static_cast<Type>(type);
    ERROR_UNLESS(out_value->IsRef(), "%s must be a reference type", desc);
    return Result::Ok;
}

Result ExprGenerator::ReadExternalKind(ExternalKind* out_value,
    const char* desc) {
    uint8_t value = 0;
    CHECK_RESULT(ReadU8(&value, desc));
    ERROR_UNLESS(value < kExternalKindCount, "invalid export external kind: %d",
        value);
    *out_value = static_cast<ExternalKind>(value);
    return Result::Ok;
}

Result ExprGenerator::ReadStr(string_view* out_str, const char* desc) {
    uint32_t str_len = 0;
    CHECK_RESULT(ReadU32Leb128(&str_len, "string length"));

    ERROR_UNLESS(state_.offset + str_len <= read_end_,
        "unable to read string: %s", desc);

    *out_str = string_view(
        reinterpret_cast<const char*>(state_.data) + state_.offset, str_len);
    state_.offset += str_len;

    ERROR_UNLESS(IsValidUtf8(out_str->data(), out_str->length()),
        "invalid utf-8 encoding: %s", desc);
    return Result::Ok;
}

Result ExprGenerator::ReadBytes(const void** out_data,
    Address* out_data_size,
    const char* desc) {
    uint32_t data_size = 0;
    CHECK_RESULT(ReadU32Leb128(&data_size, "data size"));

    ERROR_UNLESS(state_.offset + data_size <= read_end_,
        "unable to read data: %s", desc);

    *out_data = static_cast<const uint8_t*>(state_.data) + state_.offset;
    *out_data_size = data_size;
    state_.offset += data_size;
    return Result::Ok;
}

Result ExprGenerator::ReadIndex(Index* index, const char* desc) {
    uint32_t value;
    CHECK_RESULT(ReadU32Leb128(&value, desc));
    *index = value;
    return Result::Ok;
}

Result ExprGenerator::ReadOffset(Offset* offset, const char* desc) {
    uint32_t value;
    CHECK_RESULT(ReadU32Leb128(&value, desc));
    *offset = value;
    return Result::Ok;
}

Result ExprGenerator::ReadAlignment(Address* alignment_log2, const char* desc) {
    uint32_t value;
    CHECK_RESULT(ReadU32Leb128(&value, desc));
    if (value >= 32) {
        PrintError("invalid %s: %u", desc, value);
        return Result::Error;
    }
    *alignment_log2 = value;
    return Result::Ok;
}

Result ExprGenerator::ReadCount(Index* count, const char* desc) {
    CHECK_RESULT(ReadIndex(count, desc));

    // This check assumes that each item follows in this section, and takes at
    // least 1 byte. It's possible that this check passes but reading fails
    // later. It is still useful to check here, though, because it early-outs
    // when an erroneous large count is used, before allocating memory for it.
    size_t section_remaining = read_end_ - state_.offset;
    if (*count > section_remaining) {
        PrintError("invalid %s %" PRIindex ", only %" PRIzd
            " bytes left in section",
            desc, *count, section_remaining);
        return Result::Error;
    }
    return Result::Ok;
}

Result ExprGenerator::ReadField(TypeMut* out_value) {
    // TODO: Reuse for global header too?
    Type field_type;
    CHECK_RESULT(ReadType(&field_type, "field type"));
    ERROR_UNLESS(IsConcreteType(field_type),
        "expected valid field type (got " PRItypecode ")",
        WABT_PRINTF_TYPE_CODE(field_type));

    uint8_t mutable_ = 0;
    CHECK_RESULT(ReadU8(&mutable_, "field mutability"));
    ERROR_UNLESS(mutable_ <= 1, "field mutability must be 0 or 1");
    out_value->type = field_type;
    out_value->mutable_ = mutable_;
    return Result::Ok;
}

bool ExprGenerator::IsConcreteType(Type type) {
    switch (type) {
    case Type::I32:
    case Type::I64:
    case Type::F32:
    case Type::F64:
        return true;

    case Type::V128:
        return options_.features.simd_enabled();

    case Type::FuncRef:
    case Type::ExternRef:
        return options_.features.reference_types_enabled();

    case Type::ExnRef:
        return options_.features.exceptions_enabled();

    default:
        return false;
    }
}

bool ExprGenerator::IsBlockType(Type type) {
    if (IsConcreteType(type) || type == Type::Void) {
        return true;
    }

    if (!(options_.features.multi_value_enabled() && type.IsIndex())) {
        return false;
    }

    return true;
}

Result ExprGenerator::ReadAddress(Address* out_value,
    Index memory,
    const char* desc) {
    ERROR_UNLESS(memory < memories.size(),
        "load/store memory %u out of range %lu", memory,
        memories.size());
    if (memories[memory].is_64) {
        return ReadU64Leb128(out_value, desc);
    }
    else {
        uint32_t val;
        Result res = ReadU32Leb128(&val, desc);
        *out_value = val;
        return res;
    }
}

} // end of namespace wasmfuzzer