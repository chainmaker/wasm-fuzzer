/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/


#pragma once

#include "expr_generator_callback.h"

#include "src/ir.h"
#include "src/common.h"

namespace wasmfuzzer {

using wabt::Result;
using wabt::Opcode;
using State = wabt::BinaryReaderDelegate::State;
using wabt::ReadBinaryOptions;

class ExprGenerator {
	// generate expression
public:
	ExprGenerator() : state_(State(nullptr, 0)), options_(ReadBinaryOptions()) {}
	Result generateExpr(Opcode opcode = Opcode::Invalid, State state = State(nullptr, 0));
//private:
	static inline ExprGeneratorCallback* delegate_ = new ExprGeneratorCallback;
	State state_;
	size_t read_end_ = 0;
	std::vector<Index> target_depths_;
	bool seen_end_opcode = false;
	Offset end_offset;
	const ReadBinaryOptions& options_;
	std::vector<Limits> memories;
	Index data_count_ = wabt::kInvalidIndex;

private:
	template <typename T>
	Result ReadT(T* out_value,
		const char* type_name,
		const char* desc) WABT_WARN_UNUSED;
	Result ReadU8(uint8_t* out_value, const char* desc) WABT_WARN_UNUSED;
	Result ReadU32(uint32_t* out_value, const char* desc) WABT_WARN_UNUSED;
	Result ReadF32(uint32_t* out_value, const char* desc) WABT_WARN_UNUSED;
	Result ReadF64(uint64_t* out_value, const char* desc) WABT_WARN_UNUSED;
	Result ReadV128(v128* out_value, const char* desc) WABT_WARN_UNUSED;
	Result ReadU32Leb128(uint32_t* out_value, const char* desc) WABT_WARN_UNUSED;
	Result ReadU64Leb128(uint64_t* out_value, const char* desc) WABT_WARN_UNUSED;
	Result ReadS32Leb128(uint32_t* out_value, const char* desc) WABT_WARN_UNUSED;
	Result ReadS64Leb128(uint64_t* out_value, const char* desc) WABT_WARN_UNUSED;
	//Result ReadU64(uint64_t* out_value, const char* desc) WABT_WARN_UNUSED;
	Result ReadType(Type* out_value, const char* desc) WABT_WARN_UNUSED;
	Result ReadRefType(Type* out_value, const char* desc) WABT_WARN_UNUSED;
	Result ReadExternalKind(ExternalKind* out_value,
		const char* desc) WABT_WARN_UNUSED;
	Result ReadStr(string_view* out_str, const char* desc) WABT_WARN_UNUSED;
	Result ReadBytes(const void** out_data,
		Address* out_data_size,
		const char* desc) WABT_WARN_UNUSED;
	Result ReadIndex(Index* index, const char* desc) WABT_WARN_UNUSED;
	Result ReadOffset(Offset* offset, const char* desc) WABT_WARN_UNUSED;
	Result ReadAlignment(Address* align_log2, const char* desc) WABT_WARN_UNUSED;
	Result ReadCount(Index* index, const char* desc) WABT_WARN_UNUSED;
	Result ReadField(TypeMut* out_value) WABT_WARN_UNUSED;
	bool IsConcreteType(Type);
	bool IsBlockType(Type);
	Result ReadAddress(Address* out_value,
		Index memory,
		const char* desc) WABT_WARN_UNUSED;
};

} // end of namespace wasmfuzzer