/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/

#pragma once



#include "src/ir.h"
#include "src/binary-reader-nop.h"

namespace wasmfuzzer {

using wabt::ExprType;
using wabt::Expr;
using wabt::Opcode;
using wabt::Module;
using State = wabt::BinaryReaderDelegate::State;
using wabt::Offset;



static bool isExportFunctionsWithNoArgumentsOnly();

class FunctionEditor {
	// function operation
public:
	FunctionEditor(Module* pm);
	bool generateStateForOpcode(Opcode opcode);
	//select a function
	uint32_t updateCurrentFunction();
	//insert an instruction
	void insertInstruction();
	//erase an instrcution
	void eraseInstruction();
	//move an instruction
	void moveInstruction();
	// add a function
	void addFunction();
	// erase a function
	void eraseFunction();
	//swap two functions
	void swapFunction();

	Module* pModule;
	uint8_t stateBuf[1024];
	Offset stateLen;
	uint32_t currentFunction;


};

class NonFunctionEditor {
	// non-function operation
public:
	NonFunctionEditor(Module* pm);
	// add a global item
	void addGlobal();
	//erase a global item
	void eraseGlobal();
	//swap two global items
	void swapGlobal();
	// add a export item 
	void addExport();
	//erase a export item
	void eraseExport();
	//swap two export items 
	void swapExport();
	//add an import item
	void addImport();
	//add a type item
	void addType();
	//add a table item
	void addTable();
	//add a memory item
	void addMemory();
	//add a data item
	void addData();
	//set start function
	void setStart();
	//erase start function
	void eraseStart();
//private:
	Module* pModule;
};

} 
// end of namespace wasmfuzzer