﻿/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/


#include "wasmfuzzer_main.h"
#include "module_writer.h"
#include "expr_generator.h"
#include "wasmfuzzer_lib.h"

using namespace std;
using namespace wabt;
using namespace wasmfuzzer;

int main()
{
	cout << "Hello CMake." << endl;
	
	Module mo;

	// new the func module field
	FuncModuleField* pfmf = new FuncModuleField;

	// new the type module field
	TypeModuleField* ptmf = new TypeModuleField;

	// new the type export field
	ExportModuleField* pemf = new ExportModuleField;
	FuncModuleField& fmf = *pfmf;
	TypeModuleField& tmf = *ptmf;
	ExportModuleField& emf = *pemf;
	Func& f = fmf.func;

	FuncType* pft = new FuncType;
	FuncType& ft = *pft;

	//Param I32 I32 ,Result I32
	ft.sig.param_types.push_back(Type::I32);
	ft.sig.param_types.push_back(Type::I32);
	ft.sig.result_types.push_back(Type::I32);
	tmf.type = unique_ptr<TypeEntry>(pft);
	
	f.decl.sig = ft.sig;

	ExprGenerator eg;
	uint32_t arg = 1;

	// generate I32Const instruction
	eg.generateExpr(Opcode::I32Const, State((uint8_t*)&arg, sizeof(arg)));
	f.exprs.push_back(std::move(eg.delegate_->pExpr));
	arg = 2;

	// generate I32Const instruction
	eg.generateExpr(Opcode::I32Const, State((uint8_t*)&arg, sizeof(arg)));
	f.exprs.push_back(std::move(eg.delegate_->pExpr));

	// generate I32Add instruction
	eg.generateExpr(Opcode::I32Add);
	f.exprs.push_back(std::move(eg.delegate_->pExpr));

	// export a item  name "add" 
	emf.export_.name = "add";
	// export a item  type Func  
	emf.export_.kind = ExternalKind::Func;
	emf.export_.var.set_index(0);

	//put funcfield,type field, export field in  the module 
	mo.AppendField(unique_ptr<FuncModuleField>(pfmf));
	mo.AppendField(unique_ptr<TypeModuleField>(ptmf));
	mo.AppendField(unique_ptr<ExportModuleField>(pemf));

	void* pmo = &mo;
    insertInstruction(&pmo);

	cout << "Hello CMake." << endl;

	
	cout << ModuleWriter()(mo, "/home/huangyuhe/Documents/add.wasm") << endl;

	
	return 0;
}
