/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/





#include "wasmfuzzer_lib.h"
#include <gtest/gtest.h>
typedef uint8_t u8;

TEST(readTest, readWasmTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    EXPECT_EQ(0, readWasmFile(wasmfile, (void**)&in_buf));

}

TEST(isWasmStructTest, isWasmStructTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    EXPECT_EQ(1, isWasmStruct((void**)&in_buf));

}


TEST(insertInstructionTest, insertInstructionTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    for(int i=0;i<10000;i++){
        EXPECT_EQ(0, insertInstruction((void**)&in_buf));
    }


}

TEST(eraseInstructionTest, eraseInstructionTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    EXPECT_EQ(0, eraseInstruction((void**)&in_buf));

}

TEST(addFunctionTest, addFunctionTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    EXPECT_EQ(0, addFunction((void**)&in_buf));

}

TEST(eraseFunctionTest, eraseFunctionTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    addFunction((void**)&in_buf);
    addFunction((void**)&in_buf);
    addFunction((void**)&in_buf);
    addFunction((void**)&in_buf);    
    EXPECT_EQ(0, eraseFunction((void**)&in_buf));

}

TEST(swapFunctionTest, swapFunctionTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    addFunction((void**)&in_buf);
    addFunction((void**)&in_buf);
    addFunction((void**)&in_buf);
    addFunction((void**)&in_buf);    
    EXPECT_EQ(0, swapFunction((void**)&in_buf));

}



TEST(moveInstructionTest, moveInstructionTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);   
    EXPECT_EQ(0, moveInstruction((void**)&in_buf));

}

TEST(addGlobalTest, addGlobalTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);   
    EXPECT_EQ(0, addGlobal((void**)&in_buf));

}

TEST(eraseGlobalTest, eraseGlobalTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    addGlobal((void**)&in_buf);
    addGlobal((void**)&in_buf);
    EXPECT_EQ(0, eraseGlobal((void**)&in_buf));

}

TEST(swapGlobalTest, swapGlobalTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    addGlobal((void**)&in_buf);
    addGlobal((void**)&in_buf);
    EXPECT_EQ(0, swapGlobal((void**)&in_buf));

}

TEST(addExportTest, addExportTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    EXPECT_EQ(0, addExport((void**)&in_buf));

}

TEST(eraseExportTest, eraseExportTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    addExport((void**)&in_buf);
    addExport((void**)&in_buf);
    addExport((void**)&in_buf);
    EXPECT_EQ(0, eraseExport((void**)&in_buf));

}

TEST(addTypeTest, addTypeTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    EXPECT_EQ(0, addType((void**)&in_buf));

}


TEST(setStartTest, setStartTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);

    EXPECT_EQ(0, setStart((void**)&in_buf));

}

TEST(eraseStartTest, eraseStartTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    setStart((void**)&in_buf);
    EXPECT_EQ(0, eraseStart((void**)&in_buf));

}

TEST(addImportTest, addImportTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    EXPECT_EQ(0, addImport((void**)&in_buf));

}

TEST(swapExportTest, swapExportTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    addExport((void**)&in_buf);
    addExport((void**)&in_buf);
    addExport((void**)&in_buf);
    EXPECT_EQ(0, swapExport((void**)&in_buf));

}

TEST(addTableTest, addTableTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    EXPECT_EQ(0, addTable((void**)&in_buf));

}

TEST(addDataTest, addDataTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    EXPECT_EQ(0, addData((void**)&in_buf));

}


TEST(freeDataPtrTest, freeDataPtrTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    EXPECT_EQ(0,freeDataPtr((void**)&in_buf));

}

TEST(addMemoryTest, addMemoryTest)
{
    u8  *in_buf;
    char * wasmfile="./input/add.wasm";
    readWasmFile(wasmfile, (void**)&in_buf);
    EXPECT_EQ(0,addMemory((void**)&in_buf));

}

// TEST(writeWasmFileTest, writeWasmFileTest)
// {
//     u8  *in_buf;
//     char * wasmfile="../input/add.wasm";
//     readWasmFile(wasmfile, (void**)&in_buf);
//     EXPECT_EQ(0,writeWasmFile(wasmfile, (void**)&in_buf));

// }