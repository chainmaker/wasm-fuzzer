/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/

#include "wasmfuzzer_lib.h"

#include "module_writer.h"
#include "module_editor.h"
#include "expr_generator.h"
#include "util.h"

#include "src/binary-reader.h"
#include "src/binary-reader-ir.h"
#include "src/ir.h"

#include <string>
#include <iostream>
#include <fstream>

using namespace wabt;
using namespace std;
using namespace wasmfuzzer;

#ifdef __cplusplus
extern "C" {
#endif

CBoolean readWasmFile(char* filename, void** ppModule) {
    //readWasmFile to memory
    Result result;
    ReadBinaryOptions options;
    string s_infile(filename);
    std::vector<uint8_t> file_data;
    result = ReadFile(s_infile.c_str(), &file_data);
    Errors errors;
    *ppModule = new WasmFuzzerFile;
    WasmFuzzerFile& wasmFuzzerFile = *(WasmFuzzerFile*)*ppModule;
    
    wasmFuzzerFile.dataLen = -1;
    wasmFuzzerFile.mutationType = NOT_DECIDED;
    //dataPtr pointer to module
    wasmFuzzerFile.dataPtr = new Module();
    try {
        result = ReadBinaryIr(s_infile.c_str(), file_data.data(), file_data.size(),
            options, &errors, (Module*)(wasmFuzzerFile.dataPtr));
    }
    catch (...) {
        result = Result::Error;
    }
    if (result) {
        delete (Module*)(wasmFuzzerFile.dataPtr);
        wasmFuzzerFile.dataLen = file_data.size();
        wasmFuzzerFile.dataPtr = new uint8_t[wasmFuzzerFile.dataLen];
        memcpy(wasmFuzzerFile.dataPtr, file_data.data(), wasmFuzzerFile.dataLen);
    }

    //return CBoolean(result);
    return CBoolean(Result::Ok);
}


//free module ptr
CBoolean freeDataPtr(void** ppModule){
    WasmFuzzerFile& wasmFuzzerFile = *(WasmFuzzerFile*)*ppModule;
    // free data ptr
    delete wasmFuzzerFile.dataPtr;
    return 0;

}


CBoolean writeWasmFile(char* filename, void** ppModule) {
    //return CBoolean(ModuleWriter()(*(Module*)*ppModule, filename));

    WasmFuzzerFile& wasmFuzzerFile = *(WasmFuzzerFile*)*ppModule;
    if (wasmFuzzerFile.dataLen == -1) {
        Result result = ModuleWriter()(*(Module*)(wasmFuzzerFile.dataPtr), filename);
        if (result == Result::Error) {
            return CBoolean(result);
        }
    }
    else {
        ofstream ofs(filename, ios::binary);
        if (!ofs.good()) {
            return CBoolean(Result::Error);
        }
        ofs.write((const char*)(wasmFuzzerFile.dataPtr), wasmFuzzerFile.dataLen);
        ofs.close();
    }
    if (wasmFuzzerFile.mutationType == NOT_DECIDED) {
        wasmFuzzerFile.mutationType = MutationType(randomInt(1, 2));
        wasmFuzzerFile.mutationPos = randomInt<uint64_t>(0, numeric_limits<uint64_t>::max());
        wasmFuzzerFile.mutationData = randomInt<uint64_t>(0, numeric_limits<uint64_t>::max());
    }
    fstream fs(filename, ios::binary | ios::out | ios::in);
    if (!fs.good()) {
        return CBoolean(Result::Error);
    }
    fs.seekg(0, ios::end);
    uint64_t fileLen = fs.tellg();
    fs.seekg(0, ios::beg);
    if (fileLen != 0) {
        switch (wasmFuzzerFile.mutationType) {
        case NONE:
            break;
        case OVERWRITE_BYTE:
            fs.seekp(wasmFuzzerFile.mutationPos % fileLen, ios::beg);
            //write file from memory
            fs.write((const char*)&(wasmFuzzerFile.mutationData), 1);
        default:
            break;
        }
    }
    fs.close();
    return CBoolean(Result::Ok);
}


CBoolean isWasmStruct(void** ppModule) {
    //Determine whether it is a wasm file
    WasmFuzzerFile& wasmFuzzerFile = *(WasmFuzzerFile*)*ppModule;
    if (wasmFuzzerFile.dataLen == -1) {
        // if datalen = -1 ,is wasm file 
        return 1;
    }
    else {
        //else not wasmfile
        return 0;
    }
}

void libwasmfuzzerTest(void** ppModule) {
    //Module& module = *(Module*)*ppModule;
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return;
    }
    Module& module = *(Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr);
    Func& f = *(module.funcs[0]);
    ExprGenerator eg;
    uint32_t arg = 3;
    eg.generateExpr(Opcode::I32Const, State((uint8_t*)&arg, sizeof(arg)));
    f.exprs.push_back(std::move(eg.delegate_->pExpr));
    eg.generateExpr(Opcode::I32Add);
    f.exprs.push_back(std::move(eg.delegate_->pExpr));
}

int insertInstruction(void** ppModule) {
    //FunctionEditor fe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        //if not wasm file , directly return 
        return 1;
    }
    FunctionEditor fe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //select a function
    fe.updateCurrentFunction();
    // insert an instruction
    fe.insertInstruction();
    return 0;
}

int eraseInstruction(void** ppModule) {
    //FunctionEditor fe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    FunctionEditor fe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //select a function
    fe.updateCurrentFunction();
    //erase an instruction
    fe.eraseInstruction();
    return 0;
}

int moveInstruction(void** ppModule) {
    //FunctionEditor fe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    FunctionEditor fe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //select a function
    fe.updateCurrentFunction();
    //move an instruction
    fe.moveInstruction();
    return 0;
}

int addFunction(void** ppModule) {
    //FunctionEditor fe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    FunctionEditor fe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    // add a function
    fe.addFunction();
    return 0;
}

int eraseFunction(void** ppModule) {
    //FunctionEditor fe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    FunctionEditor fe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //erase a function
    fe.eraseFunction();
    return 0;
}

int swapFunction(void** ppModule) {
    //FunctionEditor fe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    FunctionEditor fe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //swap two functions
    fe.swapFunction();
    return 0;
}

int addGlobal(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //add a global item
    nfe.addGlobal();
    return 0;
}

int eraseGlobal(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    // erase a global item
    nfe.eraseGlobal();
    return 0;
}

int swapGlobal(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //swap two global items
    nfe.swapGlobal();
    return 0;
}

int addExport(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    // add a export item
    nfe.addExport();
    return 0;
}

int eraseExport(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //erase an export item
    nfe.eraseExport();
    return 0;
}

int swapExport(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //swap two export items
    nfe.swapExport();
    return 0;
}

int addImport(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //add an import item
    nfe.addImport();
    return 0;
}

int addType(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //add a type item
    nfe.addType();
    return 0;
}

int addTable(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1; 
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    // add a table item
    nfe.addTable();
    return 0;
}

int addMemory(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    // add a memory item
    nfe.addMemory();
    return 0;
}

int addData(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    // add a data item
    nfe.addData();
    return 0;
}

int setStart(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1;
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    // set start 
    nfe.setStart();
    return 0;
}

int eraseStart(void** ppModule) {
    //NonFunctionEditor nfe((Module*)*ppModule);
    if (((WasmFuzzerFile*)*ppModule)->dataLen >= 0) {
        return 1; 
    }
    NonFunctionEditor nfe((Module*)(((WasmFuzzerFile*)*ppModule)->dataPtr));
    //erase start
    nfe.eraseStart();
    return 0;
}

#ifdef __cplusplus
};
#endif