/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/

#include "expr_generator_callback.h"

namespace wasmfuzzer {

using namespace wabt;

int _PrintError_EXPR_GENERATOR_CALLBACK(const char* fmt, ...) {
    return 0;
}

#define PrintError _PrintError_EXPR_GENERATOR_CALLBACK

LabelNode::LabelNode(LabelType label_type, ExprList* exprs, Expr* context)
    : label_type(label_type), exprs(exprs), context(context) {}

Result ExprGeneratorCallback::AppendExpr(std::unique_ptr<Expr> expr) {
    pExpr = std::move(expr);
    return Result::Ok;
}

Location ExprGeneratorCallback::GetLocation() {
    //get location
    return Location();
}

void ExprGeneratorCallback::PushLabel(LabelType label_type,
    ExprList* first,
    Expr* context) {
    label_stack_.emplace_back(label_type, first, context);
}

Result ExprGeneratorCallback::PopLabel() {
    if (label_stack_.size() == 0) {
        PrintError("popping empty label stack");
        return Result::Error;
    }

    label_stack_.pop_back();
    return Result::Ok;
}

Result ExprGeneratorCallback::GetLabelAt(LabelNode** label, Index depth) {
    //get the label at any depth 
    if (depth >= label_stack_.size()) {
        //if depth >= lable_stack size ,return error
        PrintError("accessing stack depth: %" PRIindex " >= max: %" PRIzd, depth,
            label_stack_.size());
        return Result::Error;
    }
    // get depth label
    *label = &label_stack_[label_stack_.size() - depth - 1];
    return Result::Ok;
}

Result ExprGeneratorCallback::TopLabel(LabelNode** label) {
    //get top label
    return GetLabelAt(label, 0);
}

Result ExprGeneratorCallback::TopLabelExpr(LabelNode** label, Expr** expr) {
    CHECK_RESULT(TopLabel(label));
    LabelNode* parent_label;
    CHECK_RESULT(GetLabelAt(&parent_label, 1));
    *expr = &parent_label->exprs->back();
    return Result::Ok;
}

void ExprGeneratorCallback::SetFuncDeclaration(FuncDeclaration* decl, Var var) {
    //set function declartion
    decl->has_func_type = true;
    decl->type_var = var;
    //if (auto* func_type = module_->GetFuncType(var)) {
    //    decl->sig = func_type->sig;
    //}
}

void ExprGeneratorCallback::SetBlockDeclaration(BlockDeclaration* decl,
    Type sig_type) {
    //set block declaration
    if (sig_type.IsIndex()) {
        Index type_index = sig_type.GetIndex();
        SetFuncDeclaration(decl, Var(type_index));
    }
    else {
        decl->has_func_type = false;
        decl->sig.param_types.clear();
        decl->sig.result_types = sig_type.GetInlineVector();
    }
}

#define BinaryReaderIR ExprGeneratorCallback

/*bool BinaryReaderIR::OnError(const Error& error) {
    errors_->push_back(error);
    return true;
}

Result BinaryReaderIR::OnTypeCount(Index count) {
    WABT_TRY
        module_->types.reserve(count);
    WABT_CATCH_BAD_ALLOC
        return Result::Ok;
}

Result BinaryReaderIR::OnFuncType(Index index,
    Index param_count,
    Type* param_types,
    Index result_count,
    Type* result_types) {
    auto field = MakeUnique<TypeModuleField>(GetLocation());
    auto func_type = MakeUnique<FuncType>();
    func_type->sig.param_types.assign(param_types, param_types + param_count);
    func_type->sig.result_types.assign(result_types, result_types + result_count);
    field->type = std::move(func_type);
    module_->AppendField(std::move(field));
    return Result::Ok;
}

Result BinaryReaderIR::OnStructType(Index index,
    Index field_count,
    TypeMut* fields) {
    auto field = MakeUnique<TypeModuleField>(GetLocation());
    auto struct_type = MakeUnique<StructType>();
    struct_type->fields.resize(field_count);
    for (Index i = 0; i < field_count; ++i) {
        struct_type->fields[i].type = fields[i].type;
        struct_type->fields[i].mutable_ = fields[i].mutable_;
    }
    field->type = std::move(struct_type);
    module_->AppendField(std::move(field));
    return Result::Ok;
}

Result BinaryReaderIR::OnArrayType(Index index, TypeMut type_mut) {
    auto field = MakeUnique<TypeModuleField>(GetLocation());
    auto array_type = MakeUnique<ArrayType>();
    array_type->field.type = type_mut.type;
    array_type->field.mutable_ = type_mut.mutable_;
    field->type = std::move(array_type);
    module_->AppendField(std::move(field));
    return Result::Ok;
}

Result BinaryReaderIR::OnImportCount(Index count) {
    WABT_TRY
        module_->imports.reserve(count);
    WABT_CATCH_BAD_ALLOC
        return Result::Ok;
}

Result BinaryReaderIR::OnImportFunc(Index import_index,
    string_view module_name,
    string_view field_name,
    Index func_index,
    Index sig_index) {
    auto import = MakeUnique<FuncImport>();
    import->module_name = module_name.to_string();
    import->field_name = field_name.to_string();
    SetFuncDeclaration(&import->func.decl, Var(sig_index, GetLocation()));
    module_->AppendField(
        MakeUnique<ImportModuleField>(std::move(import), GetLocation()));
    return Result::Ok;
}

Result BinaryReaderIR::OnImportTable(Index import_index,
    string_view module_name,
    string_view field_name,
    Index table_index,
    Type elem_type,
    const Limits* elem_limits) {
    auto import = MakeUnique<TableImport>();
    import->module_name = module_name.to_string();
    import->field_name = field_name.to_string();
    import->table.elem_limits = *elem_limits;
    import->table.elem_type = elem_type;
    module_->AppendField(
        MakeUnique<ImportModuleField>(std::move(import), GetLocation()));
    return Result::Ok;
}

Result BinaryReaderIR::OnImportMemory(Index import_index,
    string_view module_name,
    string_view field_name,
    Index memory_index,
    const Limits* page_limits) {
    auto import = MakeUnique<MemoryImport>();
    import->module_name = module_name.to_string();
    import->field_name = field_name.to_string();
    import->memory.page_limits = *page_limits;
    module_->AppendField(
        MakeUnique<ImportModuleField>(std::move(import), GetLocation()));
    return Result::Ok;
}

Result BinaryReaderIR::OnImportGlobal(Index import_index,
    string_view module_name,
    string_view field_name,
    Index global_index,
    Type type,
    bool mutable_) {
    auto import = MakeUnique<GlobalImport>();
    import->module_name = module_name.to_string();
    import->field_name = field_name.to_string();
    import->global.type = type;
    import->global.mutable_ = mutable_;
    module_->AppendField(
        MakeUnique<ImportModuleField>(std::move(import), GetLocation()));
    return Result::Ok;
}

Result BinaryReaderIR::OnImportEvent(Index import_index,
    string_view module_name,
    string_view field_name,
    Index event_index,
    Index sig_index) {
    auto import = MakeUnique<EventImport>();
    import->module_name = module_name.to_string();
    import->field_name = field_name.to_string();
    SetFuncDeclaration(&import->event.decl, Var(sig_index, GetLocation()));
    module_->AppendField(
        MakeUnique<ImportModuleField>(std::move(import), GetLocation()));
    return Result::Ok;
}

Result BinaryReaderIR::OnFunctionCount(Index count) {
    WABT_TRY
        module_->funcs.reserve(module_->num_func_imports + count);
    WABT_CATCH_BAD_ALLOC
        return Result::Ok;
}

Result BinaryReaderIR::OnFunction(Index index, Index sig_index) {
    auto field = MakeUnique<FuncModuleField>(GetLocation());
    Func& func = field->func;
    SetFuncDeclaration(&func.decl, Var(sig_index, GetLocation()));
    module_->AppendField(std::move(field));
    return Result::Ok;
}

Result BinaryReaderIR::OnTableCount(Index count) {
    WABT_TRY
        module_->tables.reserve(module_->num_table_imports + count);
    WABT_CATCH_BAD_ALLOC
        return Result::Ok;
}

Result BinaryReaderIR::OnTable(Index index,
    Type elem_type,
    const Limits* elem_limits) {
    auto field = MakeUnique<TableModuleField>(GetLocation());
    Table& table = field->table;
    table.elem_limits = *elem_limits;
    table.elem_type = elem_type;
    module_->AppendField(std::move(field));
    return Result::Ok;
}

Result BinaryReaderIR::OnMemoryCount(Index count) {
    WABT_TRY
        module_->memories.reserve(module_->num_memory_imports + count);
    WABT_CATCH_BAD_ALLOC
        return Result::Ok;
}

Result BinaryReaderIR::OnMemory(Index index, const Limits* page_limits) {
    auto field = MakeUnique<MemoryModuleField>(GetLocation());
    Memory& memory = field->memory;
    memory.page_limits = *page_limits;
    module_->AppendField(std::move(field));
    return Result::Ok;
}

Result BinaryReaderIR::OnGlobalCount(Index count) {
    WABT_TRY
        module_->globals.reserve(module_->num_global_imports + count);
    WABT_CATCH_BAD_ALLOC
        return Result::Ok;
}

Result BinaryReaderIR::BeginGlobal(Index index, Type type, bool mutable_) {
    auto field = MakeUnique<GlobalModuleField>(GetLocation());
    Global& global = field->global;
    global.type = type;
    global.mutable_ = mutable_;
    module_->AppendField(std::move(field));
    return Result::Ok;
}

Result BinaryReaderIR::BeginGlobalInitExpr(Index index) {
    assert(index == module_->globals.size() - 1);
    Global* global = module_->globals[index];
    current_init_expr_ = &global->init_expr;
    return Result::Ok;
}

Result BinaryReaderIR::EndGlobalInitExpr(Index index) {
    current_init_expr_ = nullptr;
    return Result::Ok;
}

Result BinaryReaderIR::OnExportCount(Index count) {
    WABT_TRY
        module_->exports.reserve(count);
    WABT_CATCH_BAD_ALLOC
        return Result::Ok;
}

Result BinaryReaderIR::OnExport(Index index,
    ExternalKind kind,
    Index item_index,
    string_view name) {
    auto field = MakeUnique<ExportModuleField>(GetLocation());
    Export& export_ = field->export_;
    export_.name = name.to_string();
    export_.var = Var(item_index, GetLocation());
    export_.kind = kind;
    module_->AppendField(std::move(field));
    return Result::Ok;
}

Result BinaryReaderIR::OnStartFunction(Index func_index) {
    Var start(func_index, GetLocation());
    module_->AppendField(MakeUnique<StartModuleField>(start, GetLocation()));
    return Result::Ok;
}

Result BinaryReaderIR::OnFunctionBodyCount(Index count) {
    assert(module_->num_func_imports + count == module_->funcs.size());
    return Result::Ok;
}

Result BinaryReaderIR::BeginFunctionBody(Index index, Offset size) {
    current_func_ = module_->funcs[index];
    PushLabel(LabelType::Func, &current_func_->exprs);
    return Result::Ok;
}

Result BinaryReaderIR::OnLocalDecl(Index decl_index, Index count, Type type) {
    current_func_->local_types.AppendDecl(type, count);
    return Result::Ok;
}*/

Result BinaryReaderIR::OnAtomicLoadExpr(Opcode opcode,
    Address alignment_log2,
    Address offset) {
    return AppendExpr(
        MakeUnique<AtomicLoadExpr>(opcode, 1 << alignment_log2, offset));
}

Result BinaryReaderIR::OnAtomicStoreExpr(Opcode opcode,
    Address alignment_log2,
    Address offset) {
    return AppendExpr(
        MakeUnique<AtomicStoreExpr>(opcode, 1 << alignment_log2, offset));
}

Result BinaryReaderIR::OnAtomicRmwExpr(Opcode opcode,
    Address alignment_log2,
    Address offset) {
    return AppendExpr(
        MakeUnique<AtomicRmwExpr>(opcode, 1 << alignment_log2, offset));
}

Result BinaryReaderIR::OnAtomicRmwCmpxchgExpr(Opcode opcode,
    Address alignment_log2,
    Address offset) {
    return AppendExpr(
        MakeUnique<AtomicRmwCmpxchgExpr>(opcode, 1 << alignment_log2, offset));
}

Result BinaryReaderIR::OnAtomicWaitExpr(Opcode opcode,
    Address alignment_log2,
    Address offset) {
    return AppendExpr(
        MakeUnique<AtomicWaitExpr>(opcode, 1 << alignment_log2, offset));
}

Result BinaryReaderIR::OnAtomicFenceExpr(uint32_t consistency_model) {
    return AppendExpr(MakeUnique<AtomicFenceExpr>(consistency_model));
}

Result BinaryReaderIR::OnAtomicNotifyExpr(Opcode opcode,
    Address alignment_log2,
    Address offset) {
    return AppendExpr(
        MakeUnique<AtomicNotifyExpr>(opcode, 1 << alignment_log2, offset));
}

Result BinaryReaderIR::OnBinaryExpr(Opcode opcode) {
    return AppendExpr(MakeUnique<BinaryExpr>(opcode));
}

Result BinaryReaderIR::OnBlockExpr(Type sig_type) {
    auto expr = MakeUnique<BlockExpr>();
    SetBlockDeclaration(&expr->block.decl, sig_type);
    ExprList* expr_list = &expr->block.exprs;
    CHECK_RESULT(AppendExpr(std::move(expr)));
    PushLabel(LabelType::Block, expr_list);
    return Result::Ok;
}

Result BinaryReaderIR::OnBrExpr(Index depth) {
    return AppendExpr(MakeUnique<BrExpr>(Var(depth)));
}

Result BinaryReaderIR::OnBrIfExpr(Index depth) {
    return AppendExpr(MakeUnique<BrIfExpr>(Var(depth)));
}

Result BinaryReaderIR::OnBrOnExnExpr(Index depth, Index event_index) {
    auto expr = MakeUnique<BrOnExnExpr>();
    expr->label_var = Var(depth);
    expr->event_var = Var(event_index);
    return AppendExpr(std::move(expr));
}

Result BinaryReaderIR::OnBrTableExpr(Index num_targets,
    Index* target_depths,
    Index default_target_depth) {
    auto expr = MakeUnique<BrTableExpr>();
    expr->default_target = Var(default_target_depth);
    expr->targets.resize(num_targets);
    for (Index i = 0; i < num_targets; ++i) {
        expr->targets[i] = Var(target_depths[i]);
    }
    return AppendExpr(std::move(expr));
}

Result BinaryReaderIR::OnCallExpr(Index func_index) {
    return AppendExpr(MakeUnique<CallExpr>(Var(func_index)));
}

Result BinaryReaderIR::OnCallIndirectExpr(Index sig_index, Index table_index) {
    auto expr = MakeUnique<CallIndirectExpr>();
    SetFuncDeclaration(&expr->decl, Var(sig_index, GetLocation()));
    expr->table = Var(table_index);
    return AppendExpr(std::move(expr));
}

Result BinaryReaderIR::OnReturnCallExpr(Index func_index) {
    return AppendExpr(MakeUnique<ReturnCallExpr>(Var(func_index)));
}

Result BinaryReaderIR::OnReturnCallIndirectExpr(Index sig_index, Index table_index) {
    auto expr = MakeUnique<ReturnCallIndirectExpr>();
    SetFuncDeclaration(&expr->decl, Var(sig_index, GetLocation()));
    expr->table = Var(table_index);
    return AppendExpr(std::move(expr));
}

Result BinaryReaderIR::OnCompareExpr(Opcode opcode) {
    return AppendExpr(MakeUnique<CompareExpr>(opcode));
}

Result BinaryReaderIR::OnConvertExpr(Opcode opcode) {
    return AppendExpr(MakeUnique<ConvertExpr>(opcode));
}

Result BinaryReaderIR::OnDropExpr() {
    return AppendExpr(MakeUnique<DropExpr>());
}

Result BinaryReaderIR::OnElseExpr() {
    LabelNode* label;
    Expr* expr;
    CHECK_RESULT(TopLabelExpr(&label, &expr));

    if (label->label_type == LabelType::If) {
        auto* if_expr = cast<IfExpr>(expr);
        if_expr->true_.end_loc = GetLocation();
        label->exprs = &if_expr->false_;
        label->label_type = LabelType::Else;
    }
    else {
        PrintError("else expression without matching if");
        return Result::Error;
    }

    return Result::Ok;
}

Result BinaryReaderIR::OnEndExpr() {
    LabelNode* label;
    Expr* expr;
    CHECK_RESULT(TopLabelExpr(&label, &expr));
    switch (label->label_type) {
    case LabelType::Block:
        cast<BlockExpr>(expr)->block.end_loc = GetLocation();
        break;
    case LabelType::Loop:
        cast<LoopExpr>(expr)->block.end_loc = GetLocation();
        break;
    case LabelType::If:
        cast<IfExpr>(expr)->true_.end_loc = GetLocation();
        break;
    case LabelType::Else:
        cast<IfExpr>(expr)->false_end_loc = GetLocation();
        break;
    case LabelType::Try:
        cast<TryExpr>(expr)->block.end_loc = GetLocation();
        break;

    case LabelType::Func:
    case LabelType::Catch:
        break;
    }

    return PopLabel();
}

Result BinaryReaderIR::OnF32ConstExpr(uint32_t value_bits) {
    return AppendExpr(
        MakeUnique<ConstExpr>(Const::F32(value_bits, GetLocation())));
}

Result BinaryReaderIR::OnF64ConstExpr(uint64_t value_bits) {
    return AppendExpr(
        MakeUnique<ConstExpr>(Const::F64(value_bits, GetLocation())));
}

Result BinaryReaderIR::OnV128ConstExpr(v128 value_bits) {
    return AppendExpr(
        MakeUnique<ConstExpr>(Const::V128(value_bits, GetLocation())));
}

Result BinaryReaderIR::OnGlobalGetExpr(Index global_index) {
    return AppendExpr(
        MakeUnique<GlobalGetExpr>(Var(global_index, GetLocation())));
}

Result BinaryReaderIR::OnLocalGetExpr(Index local_index) {
    return AppendExpr(MakeUnique<LocalGetExpr>(Var(local_index, GetLocation())));
}

Result BinaryReaderIR::OnI32ConstExpr(uint32_t value) {
    return AppendExpr(MakeUnique<ConstExpr>(Const::I32(value, GetLocation())));
}

Result BinaryReaderIR::OnI64ConstExpr(uint64_t value) {
    return AppendExpr(MakeUnique<ConstExpr>(Const::I64(value, GetLocation())));
}

Result BinaryReaderIR::OnIfExpr(Type sig_type) {
    auto expr = MakeUnique<IfExpr>();
    SetBlockDeclaration(&expr->true_.decl, sig_type);
    ExprList* expr_list = &expr->true_.exprs;
    CHECK_RESULT(AppendExpr(std::move(expr)));
    PushLabel(LabelType::If, expr_list);
    return Result::Ok;
}

Result BinaryReaderIR::OnLoadExpr(Opcode opcode,
    Address alignment_log2,
    Address offset) {
    return AppendExpr(MakeUnique<LoadExpr>(opcode, 1 << alignment_log2, offset));
}

Result BinaryReaderIR::OnLoopExpr(Type sig_type) {
    auto expr = MakeUnique<LoopExpr>();
    SetBlockDeclaration(&expr->block.decl, sig_type);
    ExprList* expr_list = &expr->block.exprs;
    CHECK_RESULT(AppendExpr(std::move(expr)));
    PushLabel(LabelType::Loop, expr_list);
    return Result::Ok;
}

Result BinaryReaderIR::OnMemoryCopyExpr() {
    return AppendExpr(MakeUnique<MemoryCopyExpr>());
}

Result BinaryReaderIR::OnDataDropExpr(Index segment) {
    return AppendExpr(MakeUnique<DataDropExpr>(Var(segment)));
}

Result BinaryReaderIR::OnMemoryFillExpr() {
    return AppendExpr(MakeUnique<MemoryFillExpr>());
}

Result BinaryReaderIR::OnMemoryGrowExpr() {
    return AppendExpr(MakeUnique<MemoryGrowExpr>());
}

Result BinaryReaderIR::OnMemoryInitExpr(Index segment) {
    return AppendExpr(MakeUnique<MemoryInitExpr>(Var(segment)));
}

Result BinaryReaderIR::OnMemorySizeExpr() {
    return AppendExpr(MakeUnique<MemorySizeExpr>());
}

Result BinaryReaderIR::OnTableCopyExpr(Index dst_index, Index src_index) {
    return AppendExpr(MakeUnique<TableCopyExpr>(Var(dst_index), Var(src_index)));
}

Result BinaryReaderIR::OnElemDropExpr(Index segment) {
    return AppendExpr(MakeUnique<ElemDropExpr>(Var(segment)));
}

Result BinaryReaderIR::OnTableInitExpr(Index segment, Index table_index) {
    return AppendExpr(MakeUnique<TableInitExpr>(Var(segment), Var(table_index)));
}

Result BinaryReaderIR::OnTableGetExpr(Index table_index) {
    return AppendExpr(MakeUnique<TableGetExpr>(Var(table_index)));
}

Result BinaryReaderIR::OnTableSetExpr(Index table_index) {
    return AppendExpr(MakeUnique<TableSetExpr>(Var(table_index)));
}

Result BinaryReaderIR::OnTableGrowExpr(Index table_index) {
    return AppendExpr(MakeUnique<TableGrowExpr>(Var(table_index)));
}

Result BinaryReaderIR::OnTableSizeExpr(Index table_index) {
    return AppendExpr(MakeUnique<TableSizeExpr>(Var(table_index)));
}

Result BinaryReaderIR::OnTableFillExpr(Index table_index) {
    return AppendExpr(MakeUnique<TableFillExpr>(Var(table_index)));
}

Result BinaryReaderIR::OnRefFuncExpr(Index func_index) {
    return AppendExpr(MakeUnique<RefFuncExpr>(Var(func_index)));
}

Result BinaryReaderIR::OnRefNullExpr(Type type) {
    return AppendExpr(MakeUnique<RefNullExpr>(type));
}

Result BinaryReaderIR::OnRefIsNullExpr() {
    return AppendExpr(MakeUnique<RefIsNullExpr>());
}

Result BinaryReaderIR::OnNopExpr() {
    return AppendExpr(MakeUnique<NopExpr>());
}

Result BinaryReaderIR::OnRethrowExpr() {
    return AppendExpr(MakeUnique<RethrowExpr>());
}

Result BinaryReaderIR::OnReturnExpr() {
    return AppendExpr(MakeUnique<ReturnExpr>());
}

Result BinaryReaderIR::OnSelectExpr(Type result_type) {
    return AppendExpr(MakeUnique<SelectExpr>(result_type.GetInlineVector()));
}

Result BinaryReaderIR::OnGlobalSetExpr(Index global_index) {
    return AppendExpr(
        MakeUnique<GlobalSetExpr>(Var(global_index, GetLocation())));
}

Result BinaryReaderIR::OnLocalSetExpr(Index local_index) {
    return AppendExpr(MakeUnique<LocalSetExpr>(Var(local_index, GetLocation())));
}

Result BinaryReaderIR::OnStoreExpr(Opcode opcode,
    Address alignment_log2,
    Address offset) {
    return AppendExpr(MakeUnique<StoreExpr>(opcode, 1 << alignment_log2, offset));
}

Result BinaryReaderIR::OnThrowExpr(Index event_index) {
    return AppendExpr(MakeUnique<ThrowExpr>(Var(event_index, GetLocation())));
}

Result BinaryReaderIR::OnLocalTeeExpr(Index local_index) {
    return AppendExpr(MakeUnique<LocalTeeExpr>(Var(local_index, GetLocation())));
}

Result BinaryReaderIR::OnTryExpr(Type sig_type) {
    auto expr_ptr = MakeUnique<TryExpr>();
    // Save expr so it can be used below, after expr_ptr has been moved.
    TryExpr* expr = expr_ptr.get();
    ExprList* expr_list = &expr->block.exprs;
    SetBlockDeclaration(&expr->block.decl, sig_type);
    CHECK_RESULT(AppendExpr(std::move(expr_ptr)));
    PushLabel(LabelType::Try, expr_list, expr);
    return Result::Ok;
}

Result BinaryReaderIR::OnCatchExpr() {
    LabelNode* label;
    CHECK_RESULT(TopLabel(&label));
    if (label->label_type != LabelType::Try) {
        PrintError("catch expression without matching try");
        return Result::Error;
    }

    LabelNode* parent_label;
    CHECK_RESULT(GetLabelAt(&parent_label, 1));

    label->label_type = LabelType::Catch;
    label->exprs = &cast<TryExpr>(&parent_label->exprs->back())->catch_;
    return Result::Ok;
}

Result BinaryReaderIR::OnUnaryExpr(Opcode opcode) {
    return AppendExpr(MakeUnique<UnaryExpr>(opcode));
}

Result BinaryReaderIR::OnTernaryExpr(Opcode opcode) {
    return AppendExpr(MakeUnique<TernaryExpr>(opcode));
}

Result BinaryReaderIR::OnUnreachableExpr() {
    return AppendExpr(MakeUnique<UnreachableExpr>());
}

/*Result BinaryReaderIR::EndFunctionBody(Index index) {
    CHECK_RESULT(PopLabel());
    current_func_ = nullptr;
    return Result::Ok;
}*/

Result BinaryReaderIR::OnSimdLaneOpExpr(Opcode opcode, uint64_t value) {
    return AppendExpr(MakeUnique<SimdLaneOpExpr>(opcode, value));
}

Result BinaryReaderIR::OnSimdShuffleOpExpr(Opcode opcode, v128 value) {
    return AppendExpr(MakeUnique<SimdShuffleOpExpr>(opcode, value));
}

Result BinaryReaderIR::OnLoadSplatExpr(Opcode opcode,
    Address alignment_log2,
    Address offset) {
    return AppendExpr(
        MakeUnique<LoadSplatExpr>(opcode, 1 << alignment_log2, offset));
}


#undef BinaryReaderIR

#undef PrintError

} 
// end of namespace wasmfuzzer