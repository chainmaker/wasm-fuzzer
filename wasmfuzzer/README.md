
# License



wasm-fuzzer is made available under the Apache License, Version 2.0 (Apache-2.0), located in the [LICENSE](./LICENSE) file.


# libWasmFuzzer

- the wasmfuzzer lib 


The operation of wasm is realized by calling the apis of wabt


`wasmfuzzer_lib.h` is the exposed api of libwasmfuzzer

`util.h` is mainly random number tools

`expr_generator.cpp`, `expr_generator.h` and `expr_generator_callback.h` generate wasm instruction

`module_editor.cpp` and `module_editor.h` modify and edit the module

`wasmfuzzer_lib.cpp` read and write wasm

