/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/


#pragma once

#include "src/common.h"
#include "src/ir.h"
#include "src/error.h"
#include "src/binary-reader-nop.h"
#include "src/cast.h"

namespace wasmfuzzer {

using wabt::BinaryReaderNop;
using wabt::Expr;
using wabt::Location;
using wabt::Result;
using wabt::Error;
using wabt::Index;
using wabt::Type;
using wabt::TypeMut;
using wabt::Limits;
using wabt::string_view;
using wabt::ExternalKind;
using wabt::Offset;
using wabt::Opcode;
using wabt::Address;
using wabt::Var;
using wabt::LabelType;
using wabt::ExprList;
using wabt::FuncDeclaration;
using wabt::BlockDeclaration;
using wabt::cast;

struct LabelNode {
    LabelNode(LabelType, ExprList* exprs, Expr* context = nullptr);

    LabelType label_type;
    ExprList* exprs;
    Expr* context;
};

class ExprGeneratorCallback : public BinaryReaderNop {
public:
    std::unique_ptr<Expr> pExpr;
    std::vector<LabelNode> label_stack_;

    void PushLabel(LabelType label_type,
        ExprList* first,
        Expr* context = nullptr);
    Result PopLabel();
    Result GetLabelAt(LabelNode** label, Index depth);
    Result TopLabel(LabelNode** label);
    Result TopLabelExpr(LabelNode** label, Expr** expr);
    Result AppendExpr(std::unique_ptr<Expr> expr);
    Location GetLocation();
    void SetFuncDeclaration(FuncDeclaration* decl, Var var);
    void SetBlockDeclaration(BlockDeclaration* decl, Type sig_type);

public:


/*    bool OnError(const Error&) override;

    Result OnTypeCount(Index count) override;
    Result OnFuncType(Index index,
        Index param_count,
        Type* param_types,
        Index result_count,
        Type* result_types) override;
    Result OnStructType(Index index, Index field_count, TypeMut* fields) override;
    Result OnArrayType(Index index, TypeMut field) override;

    Result OnImportCount(Index count) override;
    Result OnImportFunc(Index import_index,
        string_view module_name,
        string_view field_name,
        Index func_index,
        Index sig_index) override;
    Result OnImportTable(Index import_index,
        string_view module_name,
        string_view field_name,
        Index table_index,
        Type elem_type,
        const Limits* elem_limits) override;
    Result OnImportMemory(Index import_index,
        string_view module_name,
        string_view field_name,
        Index memory_index,
        const Limits* page_limits) override;
    Result OnImportGlobal(Index import_index,
        string_view module_name,
        string_view field_name,
        Index global_index,
        Type type,
        bool mutable_) override;
    Result OnImportEvent(Index import_index,
        string_view module_name,
        string_view field_name,
        Index event_index,
        Index sig_index) override;

    Result OnFunctionCount(Index count) override;
    Result OnFunction(Index index, Index sig_index) override;

    Result OnTableCount(Index count) override;
    Result OnTable(Index index,
        Type elem_type,
        const Limits* elem_limits) override;

    Result OnMemoryCount(Index count) override;
    Result OnMemory(Index index, const Limits* limits) override;

    Result OnGlobalCount(Index count) override;
    Result BeginGlobal(Index index, Type type, bool mutable_) override;
    Result BeginGlobalInitExpr(Index index) override;
    Result EndGlobalInitExpr(Index index) override;

    Result OnExportCount(Index count) override;
    Result OnExport(Index index,
        ExternalKind kind,
        Index item_index,
        string_view name) override;

    Result OnStartFunction(Index func_index) override;

    Result OnFunctionBodyCount(Index count) override;
    Result BeginFunctionBody(Index index, Offset size) override;
    Result OnLocalDecl(Index decl_index, Index count, Type type) override;*/

    Result OnAtomicLoadExpr(Opcode opcode,
        Address alignment_log2,
        Address offset) override;
    Result OnAtomicStoreExpr(Opcode opcode,
        Address alignment_log2,
        Address offset) override;
    Result OnAtomicRmwExpr(Opcode opcode,
        Address alignment_log2,
        Address offset) override;
    Result OnAtomicRmwCmpxchgExpr(Opcode opcode,
        Address alignment_log2,
        Address offset) override;
    Result OnAtomicWaitExpr(Opcode opcode,
        Address alignment_log2,
        Address offset) override;
    Result OnAtomicFenceExpr(uint32_t consistency_model) override;
    Result OnAtomicNotifyExpr(Opcode opcode,
        Address alignment_log2,
        Address offset) override;
    Result OnBinaryExpr(Opcode opcode) override;
    Result OnBlockExpr(Type sig_type) override;
    Result OnBrExpr(Index depth) override;
    Result OnBrIfExpr(Index depth) override;
    Result OnBrOnExnExpr(Index depth, Index event_index) override;
    Result OnBrTableExpr(Index num_targets,
        Index* target_depths,
        Index default_target_depth) override;
    Result OnCallExpr(Index func_index) override;
    Result OnCatchExpr() override;
    Result OnCallIndirectExpr(Index sig_index, Index table_index) override;
    Result OnReturnCallExpr(Index func_index) override;
    Result OnReturnCallIndirectExpr(Index sig_index, Index table_index) override;
    Result OnCompareExpr(Opcode opcode) override;
    Result OnConvertExpr(Opcode opcode) override;
    Result OnDropExpr() override;
    Result OnElseExpr() override;
    Result OnEndExpr() override;
    Result OnF32ConstExpr(uint32_t value_bits) override;
    Result OnF64ConstExpr(uint64_t value_bits) override;
    Result OnV128ConstExpr(v128 value_bits) override;
    Result OnGlobalGetExpr(Index global_index) override;
    Result OnGlobalSetExpr(Index global_index) override;
    Result OnI32ConstExpr(uint32_t value) override;
    Result OnI64ConstExpr(uint64_t value) override;
    Result OnIfExpr(Type sig_type) override;
    Result OnLoadExpr(Opcode opcode,
        Address alignment_log2,
        Address offset) override;
    Result OnLocalGetExpr(Index local_index) override;
    Result OnLocalSetExpr(Index local_index) override;
    Result OnLocalTeeExpr(Index local_index) override;
    Result OnLoopExpr(Type sig_type) override;
    Result OnMemoryCopyExpr() override;
    Result OnDataDropExpr(Index segment_index) override;
    Result OnMemoryFillExpr() override;
    Result OnMemoryGrowExpr() override;
    Result OnMemoryInitExpr(Index segment_index) override;
    Result OnMemorySizeExpr() override;
    Result OnTableCopyExpr(Index dst_index, Index src_index) override;
    Result OnElemDropExpr(Index segment_index) override;
    Result OnTableInitExpr(Index segment_index, Index table_index) override;
    Result OnTableGetExpr(Index table_index) override;
    Result OnTableSetExpr(Index table_index) override;
    Result OnTableGrowExpr(Index table_index) override;
    Result OnTableSizeExpr(Index table_index) override;
    Result OnTableFillExpr(Index table_index) override;
    Result OnRefFuncExpr(Index func_index) override;
    Result OnRefNullExpr(Type type) override;
    Result OnRefIsNullExpr() override;
    Result OnNopExpr() override;
    Result OnRethrowExpr() override;
    Result OnReturnExpr() override;
    Result OnSelectExpr(Type result_type) override;
    Result OnStoreExpr(Opcode opcode,
        Address alignment_log2,
        Address offset) override;
    Result OnThrowExpr(Index event_index) override;
    Result OnTryExpr(Type sig_type) override;
    Result OnUnaryExpr(Opcode opcode) override;
    Result OnTernaryExpr(Opcode opcode) override;
    Result OnUnreachableExpr() override;
    //Result EndFunctionBody(Index index) override;
    Result OnSimdLaneOpExpr(Opcode opcode, uint64_t value) override;
    Result OnSimdShuffleOpExpr(Opcode opcode, v128 value) override;
    Result OnLoadSplatExpr(Opcode opcode,
        Address alignment_log2,
        Address offset) override;


};

} // end of namespace wasmfuzzer