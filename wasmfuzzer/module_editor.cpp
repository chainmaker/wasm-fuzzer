/*

Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

SPDX-License-Identifier: Apache-2.0

*/


#include "module_editor.h"
#include "util.h"
#include "expr_generator.h"

#include "src/leb128.h"
#include "src/stream.h"

#include <cstdlib>

namespace wasmfuzzer {

using namespace wabt;

#ifdef WASMFUZZER_EXPORT_FUNCTIONS_WITH_NO_ARGUMENTS_ONLY

static int16_t exportFunctionsWithNoArgumentsOnly = -1;

static bool isExportFunctionsWithNoArgumentsOnly() {
    if(exportFunctionsWithNoArgumentsOnly != -1) {
        return bool(exportFunctionsWithNoArgumentsOnly);
    }
    else {
        return (exportFunctionsWithNoArgumentsOnly = bool(std::getenv("WASMFUZZER_EXPORT_FUNCTIONS_WITH_NO_ARGUMENTS_ONLY")));
    }
}

#endif // WASMFUZZER_EXPORT_FUNCTIONS_WITH_NO_ARGUMENTS_ONLY

FunctionEditor::FunctionEditor(Module* pm) : pModule(pm), stateLen(0), currentFunction(0) {
    //init stateBuf
    memset(stateBuf, 0, sizeof(stateBuf));
}

bool FunctionEditor::generateStateForOpcode(Opcode opcode)
{
    bool success = true;
    switch (opcode) {
    case Opcode::Unreachable:
        stateLen = 0;
        break;

    case Opcode::Block: {
        int32_t tp = randomInt<int32_t>(-4, 0);
        if(tp == 0) {
            tp = -64;
        }
        MemoryStream tmpStream;
        WriteS32Leb128(&tmpStream, tp, nullptr);
        stateLen = tmpStream.offset();
        memcpy(stateBuf, &(tmpStream.output_buffer().data[0]), stateLen);
        break;
    }

    case Opcode::Loop: {
        int32_t tp = randomInt<int32_t>(-4, 0);
        if(tp == 0) {
            tp = -64;
        }
        MemoryStream tmpStream;
        WriteS32Leb128(&tmpStream, tp, nullptr);
        stateLen = tmpStream.offset();
        memcpy(stateBuf, &(tmpStream.output_buffer().data[0]), stateLen);
        break;
    }

    case Opcode::If: {
        int32_t tp = randomInt<int32_t>(-4, 0);
        if(tp == 0) {
            tp = -64;
        }
        MemoryStream tmpStream;
        WriteS32Leb128(&tmpStream, tp, nullptr);
        stateLen = tmpStream.offset();
        memcpy(stateBuf, &(tmpStream.output_buffer().data[0]), stateLen);
        break;
    }

    case Opcode::Else: {
        success = false;
        stateLen = 0;
        break;
    }

    case Opcode::SelectT: {

        success = false;
        break;
    }

    case Opcode::Select: {
        int32_t tp = randomInt<int32_t>(-4, 0);
        if(tp == 0) {
            tp = -64;
        }
        MemoryStream tmpStream;
        WriteS32Leb128(&tmpStream, tp, nullptr);
        stateLen = tmpStream.offset();
        memcpy(stateBuf, &(tmpStream.output_buffer().data[0]), stateLen);
        break;
    }

    case Opcode::Br: {
        int32_t tp = randomInt<int32_t>(-4, 0);
        if(tp == 0) {
            tp = -64;
        }
        MemoryStream tmpStream;
        WriteS32Leb128(&tmpStream, tp, nullptr);
        stateLen = tmpStream.offset();
        memcpy(stateBuf, &(tmpStream.output_buffer().data[0]), stateLen);
        break;
    }

    case Opcode::BrIf: {
        int32_t tp = randomInt<int32_t>(-4, 0);
        if(tp == 0) {
            tp = -64;
        }
        MemoryStream tmpStream;
        WriteS32Leb128(&tmpStream, tp, nullptr);
        stateLen = tmpStream.offset();
        memcpy(stateBuf, &(tmpStream.output_buffer().data[0]), stateLen);
        break;
    }
/*
    case Opcode::BrTable: {
        Index num_targets;
        CHECK_RESULT(ReadCount(&num_targets, "br_table target count"));
        target_depths_.resize(num_targets);

        for (Index i = 0; i < num_targets; ++i) {
            Index target_depth;
            CHECK_RESULT(ReadIndex(&target_depth, "br_table target depth"));
            target_depths_[i] = target_depth;
        }

        Index default_target_depth;
        CHECK_RESULT(
            ReadIndex(&default_target_depth, "br_table default target depth"));

        Index* target_depths = num_targets ? target_depths_.data() : nullptr;

        CALLBACK(OnBrTableExpr, num_targets, target_depths,
            default_target_depth);
        break;
    }*/

    case Opcode::Return:
        stateLen = 0;
        break;

    case Opcode::Nop:
        stateLen = 0;
        break;

    case Opcode::Drop:
        stateLen = 0;
        break;
        
    case Opcode::End:
        success = false;
        stateLen = 0;
        break;

    case Opcode::I32Const: {
        //*(uint32_t*)stateBuf = getRandom<uint32_t>();
        //stateLen = sizeof(uint32_t);
        uint32_t tmpValue = getRandom<uint32_t>();
        MemoryStream tmpStream;
        WriteU32Leb128(&tmpStream, tmpValue, nullptr);
        stateLen = tmpStream.offset();
        memcpy(stateBuf, &(tmpStream.output_buffer().data[0]), stateLen);
        break;
    }

    case Opcode::I64Const: {
        *(uint64_t*)stateBuf = getRandom<uint64_t>();
        stateLen = sizeof(uint64_t);
        break;
    }

    case Opcode::F32Const: {
        *(float*)stateBuf = getRandom<float>();
        stateLen = sizeof(float);
        break;
    }

    case Opcode::F64Const: {
        *(double*)stateBuf = getRandom<double>();
        stateLen = sizeof(double);
        break;
    }

    case Opcode::V128Const: {
        *(uint64_t*)stateBuf = getRandom<uint64_t>();
        *(uint64_t*)(stateBuf + 1) = getRandom<uint64_t>();
        stateLen = 2 * sizeof(uint64_t);
        break;
    }

    case Opcode::GlobalGet: {
        uint32_t cnt = pModule->globals.size();
        if (cnt == 0) {
            success = false;
        }
        else {
            *(uint32_t*)stateBuf = randomInt<uint32_t>(0, cnt);
            stateLen = sizeof(uint32_t);
        }
        break;
    }

    case Opcode::LocalGet: {
        uint32_t cnt = pModule->funcs[currentFunction]->decl.sig.param_types.size();
        if (cnt == 0) {
            success = false;
        }
        else {
            *(uint32_t*)stateBuf = randomInt<uint32_t>(0, cnt);
            stateLen = sizeof(uint32_t);
        }
        break;
    }

    case Opcode::GlobalSet: {
        uint32_t cnt = pModule->globals.size();
        if (cnt == 0) {
            success = false;
        }
        else {
            *(uint32_t*)stateBuf = randomInt<uint32_t>(0, cnt);
            stateLen = sizeof(uint32_t);
        }
        break;
    }

    case Opcode::LocalSet: {
        uint32_t cnt = pModule->funcs[currentFunction]->decl.sig.param_types.size();
        if (cnt == 0) {
            success = false;
        }
        else {
            *(uint32_t*)stateBuf = randomInt<uint32_t>(0, cnt);
            stateLen = sizeof(uint32_t);
        }
        break;
    }

    case Opcode::Call: {
        uint32_t cnt = pModule->funcs.size();
        if (cnt == 0) {
            success = false;
        }
        else {
            *(uint32_t*)stateBuf = randomInt<uint32_t>(0, cnt);
            stateLen = sizeof(uint32_t);
        }
        break;
    }

    case Opcode::CallIndirect: {
        uint32_t cnt = pModule->tables.size();
        if (cnt == 0) {
            success = false;
        }
        else {
            *(uint32_t*)stateBuf = randomInt<uint32_t>(0, cnt);
            stateLen = sizeof(uint32_t);
        }
        break;
    }

    case Opcode::ReturnCall: {
        // NOT existed OP
        success = false;
        break;
    }

    case Opcode::ReturnCallIndirect: {
        uint32_t cnt = pModule->tables.size();
        if (cnt == 0) {
            success = false;
        }
        else {
            *(uint32_t*)stateBuf = randomInt<uint32_t>(0, cnt);
            stateLen = sizeof(uint32_t);
        }
        break;
    }

    case Opcode::LocalTee: {
        uint32_t cnt = pModule->funcs[currentFunction]->decl.sig.param_types.size();
        if (cnt == 0) {
            success = false;
        }
        else {
            *(uint32_t*)stateBuf = randomInt<uint32_t>(0, cnt);
            stateLen = sizeof(uint32_t);
        }
        break;
    }

    case Opcode::I32Load8S:
    case Opcode::I32Load8U:
    case Opcode::I32Load16S:
    case Opcode::I32Load16U:
    case Opcode::I64Load8S:
    case Opcode::I64Load8U:
    case Opcode::I64Load16S:
    case Opcode::I64Load16U:
    case Opcode::I64Load32S:
    case Opcode::I64Load32U:
    case Opcode::I32Load:
    case Opcode::I64Load:
    case Opcode::F32Load:
    case Opcode::F64Load:
    case Opcode::V128Load:
    case Opcode::V128Load8X8S:
    case Opcode::V128Load8X8U:
    case Opcode::V128Load16X4S:
    case Opcode::V128Load16X4U:
    case Opcode::V128Load32X2S:
    case Opcode::V128Load32X2U: {
        if (pModule->memories.size() == 0) {
            success = false;
        }
        else {
            success = false;
            /*uint64_t maxSize = pModule->memories[0]->page_limits.max;
            uint32_t alignment = (1 << 12);
            uint64_t offset = randomInt<uint64_t>(0ULL, maxSize);
            *(uint32_t*)stateBuf = alignment;
            *(uint64_t*)(((uint32_t*)stateBuf) + 1) = offset;
            stateLen = sizeof(uint32_t) + sizeof(uint64_t);*/
        }
        break;
    }

    case Opcode::I32Store8:
    case Opcode::I32Store16:
    case Opcode::I64Store8:
    case Opcode::I64Store16:
    case Opcode::I64Store32:
    case Opcode::I32Store:
    case Opcode::I64Store:
    case Opcode::F32Store:
    case Opcode::F64Store:
    case Opcode::V128Store: {
        if (pModule->memories.size() == 0) {
            success = false;
        }
        else {
            success = false;
            /*uint64_t maxSize = pModule->memories[0]->page_limits.max;
            uint32_t alignment = (1 << 12);
            uint64_t offset = randomInt<uint64_t>(0, maxSize);
            *(uint32_t*)stateBuf = alignment;
            *(uint64_t*)(((uint32_t*)stateBuf) + 1) = offset;
            stateLen = sizeof(uint32_t) + sizeof(uint64_t);*/
        }
        break;
    }

    case Opcode::MemorySize: {
        *(uint32_t*)stateBuf = 0;
        stateLen = sizeof(uint32_t);
        break;
    }

    case Opcode::MemoryGrow: {
        uint64_t pageSize = 65536;
        uint64_t pageCnt = randomInt(1, 1024);
        *(uint64_t*)stateBuf = pageSize * pageCnt;
        stateLen = sizeof(uint64_t);
        break;
    }

    case Opcode::I32Add:
    case Opcode::I32Sub:
    case Opcode::I32Mul:
    case Opcode::I32DivS:
    case Opcode::I32DivU:
    case Opcode::I32RemS:
    case Opcode::I32RemU:
    case Opcode::I32And:
    case Opcode::I32Or:
    case Opcode::I32Xor:
    case Opcode::I32Shl:
    case Opcode::I32ShrU:
    case Opcode::I32ShrS:
    case Opcode::I32Rotr:
    case Opcode::I32Rotl:
    case Opcode::I64Add:
    case Opcode::I64Sub:
    case Opcode::I64Mul:
    case Opcode::I64DivS:
    case Opcode::I64DivU:
    case Opcode::I64RemS:
    case Opcode::I64RemU:
    case Opcode::I64And:
    case Opcode::I64Or:
    case Opcode::I64Xor:
    case Opcode::I64Shl:
    case Opcode::I64ShrU:
    case Opcode::I64ShrS:
    case Opcode::I64Rotr:
    case Opcode::I64Rotl:
    case Opcode::F32Add:
    case Opcode::F32Sub:
    case Opcode::F32Mul:
    case Opcode::F32Div:
    case Opcode::F32Min:
    case Opcode::F32Max:
    case Opcode::F32Copysign:
    case Opcode::F64Add:
    case Opcode::F64Sub:
    case Opcode::F64Mul:
    case Opcode::F64Div:
    case Opcode::F64Min:
    case Opcode::F64Max:
    case Opcode::F64Copysign:
    case Opcode::I8X16Add:
    case Opcode::I16X8Add:
    case Opcode::I32X4Add:
    case Opcode::I64X2Add:
    case Opcode::I8X16Sub:
    case Opcode::I16X8Sub:
    case Opcode::I32X4Sub:
    case Opcode::I64X2Sub:
    case Opcode::I16X8Mul:
    case Opcode::I32X4Mul:
    case Opcode::I64X2Mul:
    case Opcode::I8X16AddSatS:
    case Opcode::I8X16AddSatU:
    case Opcode::I16X8AddSatS:
    case Opcode::I16X8AddSatU:
    case Opcode::I8X16SubSatS:
    case Opcode::I8X16SubSatU:
    case Opcode::I16X8SubSatS:
    case Opcode::I16X8SubSatU:
    case Opcode::I8X16MinS:
    case Opcode::I16X8MinS:
    case Opcode::I32X4MinS:
    case Opcode::I8X16MinU:
    case Opcode::I16X8MinU:
    case Opcode::I32X4MinU:
    case Opcode::I8X16MaxS:
    case Opcode::I16X8MaxS:
    case Opcode::I32X4MaxS:
    case Opcode::I8X16MaxU:
    case Opcode::I16X8MaxU:
    case Opcode::I32X4MaxU:
    case Opcode::I8X16Shl:
    case Opcode::I16X8Shl:
    case Opcode::I32X4Shl:
    case Opcode::I64X2Shl:
    case Opcode::I8X16ShrS:
    case Opcode::I8X16ShrU:
    case Opcode::I16X8ShrS:
    case Opcode::I16X8ShrU:
    case Opcode::I32X4ShrS:
    case Opcode::I32X4ShrU:
    case Opcode::I64X2ShrS:
    case Opcode::I64X2ShrU:
    case Opcode::V128And:
    case Opcode::V128Or:
    case Opcode::V128Xor:
    case Opcode::F32X4Min:
    case Opcode::F32X4PMin:
    case Opcode::F64X2Min:
    case Opcode::F64X2PMin:
    case Opcode::F32X4Max:
    case Opcode::F32X4PMax:
    case Opcode::F64X2Max:
    case Opcode::F64X2PMax:
    case Opcode::F32X4Add:
    case Opcode::F64X2Add:
    case Opcode::F32X4Sub:
    case Opcode::F64X2Sub:
    case Opcode::F32X4Div:
    case Opcode::F64X2Div:
    case Opcode::F32X4Mul:
    case Opcode::F64X2Mul:
    case Opcode::I8X16Swizzle:
    case Opcode::I8X16NarrowI16X8S:
    case Opcode::I8X16NarrowI16X8U:
    case Opcode::I16X8NarrowI32X4S:
    case Opcode::I16X8NarrowI32X4U:
    case Opcode::V128Andnot:
    case Opcode::I8X16AvgrU:
    case Opcode::I16X8AvgrU:
        stateLen = 0;
        break;

    case Opcode::I32Eq:
    case Opcode::I32Ne:
    case Opcode::I32LtS:
    case Opcode::I32LeS:
    case Opcode::I32LtU:
    case Opcode::I32LeU:
    case Opcode::I32GtS:
    case Opcode::I32GeS:
    case Opcode::I32GtU:
    case Opcode::I32GeU:
    case Opcode::I64Eq:
    case Opcode::I64Ne:
    case Opcode::I64LtS:
    case Opcode::I64LeS:
    case Opcode::I64LtU:
    case Opcode::I64LeU:
    case Opcode::I64GtS:
    case Opcode::I64GeS:
    case Opcode::I64GtU:
    case Opcode::I64GeU:
    case Opcode::F32Eq:
    case Opcode::F32Ne:
    case Opcode::F32Lt:
    case Opcode::F32Le:
    case Opcode::F32Gt:
    case Opcode::F32Ge:
    case Opcode::F64Eq:
    case Opcode::F64Ne:
    case Opcode::F64Lt:
    case Opcode::F64Le:
    case Opcode::F64Gt:
    case Opcode::F64Ge:
    case Opcode::I8X16Eq:
    case Opcode::I16X8Eq:
    case Opcode::I32X4Eq:
    case Opcode::F32X4Eq:
    case Opcode::F64X2Eq:
    case Opcode::I8X16Ne:
    case Opcode::I16X8Ne:
    case Opcode::I32X4Ne:
    case Opcode::F32X4Ne:
    case Opcode::F64X2Ne:
    case Opcode::I8X16LtS:
    case Opcode::I8X16LtU:
    case Opcode::I16X8LtS:
    case Opcode::I16X8LtU:
    case Opcode::I32X4LtS:
    case Opcode::I32X4LtU:
    case Opcode::F32X4Lt:
    case Opcode::F64X2Lt:
    case Opcode::I8X16LeS:
    case Opcode::I8X16LeU:
    case Opcode::I16X8LeS:
    case Opcode::I16X8LeU:
    case Opcode::I32X4LeS:
    case Opcode::I32X4LeU:
    case Opcode::F32X4Le:
    case Opcode::F64X2Le:
    case Opcode::I8X16GtS:
    case Opcode::I8X16GtU:
    case Opcode::I16X8GtS:
    case Opcode::I16X8GtU:
    case Opcode::I32X4GtS:
    case Opcode::I32X4GtU:
    case Opcode::F32X4Gt:
    case Opcode::F64X2Gt:
    case Opcode::I8X16GeS:
    case Opcode::I8X16GeU:
    case Opcode::I16X8GeS:
    case Opcode::I16X8GeU:
    case Opcode::I32X4GeS:
    case Opcode::I32X4GeU:
    case Opcode::F32X4Ge:
    case Opcode::F64X2Ge:
        stateLen = 0;
        break;

    case Opcode::I32Clz:
    case Opcode::I32Ctz:
    case Opcode::I32Popcnt:
    case Opcode::I64Clz:
    case Opcode::I64Ctz:
    case Opcode::I64Popcnt:
    case Opcode::F32Abs:
    case Opcode::F32Neg:
    case Opcode::F32Ceil:
    case Opcode::F32Floor:
    case Opcode::F32Trunc:
    case Opcode::F32Nearest:
    case Opcode::F32Sqrt:
    case Opcode::F64Abs:
    case Opcode::F64Neg:
    case Opcode::F64Ceil:
    case Opcode::F64Floor:
    case Opcode::F64Trunc:
    case Opcode::F64Nearest:
    case Opcode::F64Sqrt:
    case Opcode::I8X16Splat:
    case Opcode::I16X8Splat:
    case Opcode::I32X4Splat:
    case Opcode::I64X2Splat:
    case Opcode::F32X4Splat:
    case Opcode::F64X2Splat:
    case Opcode::I8X16Neg:
    case Opcode::I16X8Neg:
    case Opcode::I32X4Neg:
    case Opcode::I64X2Neg:
    case Opcode::V128Not:
    case Opcode::I8X16AnyTrue:
    case Opcode::I16X8AnyTrue:
    case Opcode::I32X4AnyTrue:
    case Opcode::I8X16Bitmask:
    case Opcode::I16X8Bitmask:
    case Opcode::I32X4Bitmask:
    case Opcode::I8X16AllTrue:
    case Opcode::I16X8AllTrue:
    case Opcode::I32X4AllTrue:
    case Opcode::F32X4Ceil:
    case Opcode::F64X2Ceil:
    case Opcode::F32X4Floor:
    case Opcode::F64X2Floor:
    case Opcode::F32X4Trunc:
    case Opcode::F64X2Trunc:
    case Opcode::F32X4Nearest:
    case Opcode::F64X2Nearest:
    case Opcode::F32X4Neg:
    case Opcode::F64X2Neg:
    case Opcode::F32X4Abs:
    case Opcode::F64X2Abs:
    case Opcode::F32X4Sqrt:
    case Opcode::F64X2Sqrt:
    case Opcode::I16X8WidenLowI8X16S:
    case Opcode::I16X8WidenHighI8X16S:
    case Opcode::I16X8WidenLowI8X16U:
    case Opcode::I16X8WidenHighI8X16U:
    case Opcode::I32X4WidenLowI16X8S:
    case Opcode::I32X4WidenHighI16X8S:
    case Opcode::I32X4WidenLowI16X8U:
    case Opcode::I32X4WidenHighI16X8U:
    case Opcode::I8X16Abs:
    case Opcode::I16X8Abs:
    case Opcode::I32X4Abs:
        stateLen = 0;
        break;

    case Opcode::V128BitSelect:
        stateLen = 0;
        break;
/*
    case Opcode::I8X16ExtractLaneS:
    case Opcode::I8X16ExtractLaneU:
    case Opcode::I16X8ExtractLaneS:
    case Opcode::I16X8ExtractLaneU:
    case Opcode::I32X4ExtractLane:
    case Opcode::I64X2ExtractLane:
    case Opcode::F32X4ExtractLane:
    case Opcode::F64X2ExtractLane:
    case Opcode::I8X16ReplaceLane:
    case Opcode::I16X8ReplaceLane:
    case Opcode::I32X4ReplaceLane:
    case Opcode::I64X2ReplaceLane:
    case Opcode::F32X4ReplaceLane:
    case Opcode::F64X2ReplaceLane: {
        uint8_t lane_val;
        CHECK_RESULT(ReadU8(&lane_val, "Lane idx"));
        CALLBACK(OnSimdLaneOpExpr, opcode, lane_val);
        CALLBACK(OnOpcodeUint64, lane_val);
        break;
    }

    case Opcode::I8X16Shuffle: {
        v128 value;
        CHECK_RESULT(ReadV128(&value, "Lane idx [16]"));
        CALLBACK(OnSimdShuffleOpExpr, opcode, value);
        CALLBACK(OnOpcodeV128, value);
        break;
    }

    case Opcode::V128Load8Splat:
    case Opcode::V128Load16Splat:
    case Opcode::V128Load32Splat:
    case Opcode::V128Load64Splat: {
        Address alignment_log2;
        CHECK_RESULT(ReadAlignment(&alignment_log2, "load alignment"));
        Address offset;
        CHECK_RESULT(ReadAddress(&offset, 0, "load offset"));

        CALLBACK(OnLoadSplatExpr, opcode, alignment_log2, offset);
        CALLBACK(OnOpcodeUint32Uint32, alignment_log2, offset);
        break;
    }*/
    case Opcode::I32TruncF32S:
    case Opcode::I32TruncF64S:
    case Opcode::I32TruncF32U:
    case Opcode::I32TruncF64U:
    case Opcode::I32WrapI64:
    case Opcode::I64TruncF32S:
    case Opcode::I64TruncF64S:
    case Opcode::I64TruncF32U:
    case Opcode::I64TruncF64U:
    case Opcode::I64ExtendI32S:
    case Opcode::I64ExtendI32U:
    case Opcode::F32ConvertI32S:
    case Opcode::F32ConvertI32U:
    case Opcode::F32ConvertI64S:
    case Opcode::F32ConvertI64U:
    case Opcode::F32DemoteF64:
    case Opcode::F32ReinterpretI32:
    case Opcode::F64ConvertI32S:
    case Opcode::F64ConvertI32U:
    case Opcode::F64ConvertI64S:
    case Opcode::F64ConvertI64U:
    case Opcode::F64PromoteF32:
    case Opcode::F64ReinterpretI64:
    case Opcode::I32ReinterpretF32:
    case Opcode::I64ReinterpretF64:
    case Opcode::I32Eqz:
    case Opcode::I64Eqz:
    case Opcode::F32X4ConvertI32X4S:
    case Opcode::F32X4ConvertI32X4U:
    case Opcode::I32X4TruncSatF32X4S:
    case Opcode::I32X4TruncSatF32X4U:
        stateLen = 0;
        break;
        /*
    case Opcode::Try: {
        Type sig_type;
        CHECK_RESULT(ReadType(&sig_type, "try signature type"));
        ERROR_UNLESS(IsBlockType(sig_type),
            "expected valid block signature type");
        CALLBACK(OnTryExpr, sig_type);
        CALLBACK(OnOpcodeBlockSig, sig_type);
        break;
    }

    case Opcode::Catch: {
        CALLBACK0(OnCatchExpr);
        CALLBACK0(OnOpcodeBare);
        break;
    }

    case Opcode::Rethrow: {
        CALLBACK0(OnRethrowExpr);
        CALLBACK0(OnOpcodeBare);
        break;
    }

    case Opcode::Throw: {
        Index index;
        CHECK_RESULT(ReadIndex(&index, "event index"));
        CALLBACK(OnThrowExpr, index);
        CALLBACK(OnOpcodeIndex, index);
        break;
    }

    case Opcode::BrOnExn: {
        Index depth;
        Index index;
        CHECK_RESULT(ReadIndex(&depth, "br_on_exn depth"));
        CHECK_RESULT(ReadIndex(&index, "event index"));
        CALLBACK(OnBrOnExnExpr, depth, index);
        CALLBACK(OnOpcodeIndexIndex, depth, index);
        break;
    }*/

    case Opcode::I32Extend8S:
    case Opcode::I32Extend16S:
    case Opcode::I64Extend8S:
    case Opcode::I64Extend16S:
    case Opcode::I64Extend32S:
        stateLen = 0;
        break;

    case Opcode::I32TruncSatF32S:
    case Opcode::I32TruncSatF32U:
    case Opcode::I32TruncSatF64S:
    case Opcode::I32TruncSatF64U:
    case Opcode::I64TruncSatF32S:
    case Opcode::I64TruncSatF32U:
    case Opcode::I64TruncSatF64S:
    case Opcode::I64TruncSatF64U:
        stateLen = 0;
        break;


    default:
        success = false;
    }
    return success;
}

uint32_t FunctionEditor::updateCurrentFunction() {
    currentFunction = randomInt(std::size_t(0), pModule->funcs.size() - 1);
    //currentFunction = 0;
    return currentFunction;
}

void FunctionEditor::insertInstruction()
{
    //const std::lock_guard<std::mutex> _lock_guard(_mutex);
    //insert a instruction
    Module& module = *pModule;
    Func& f = *(module.funcs[currentFunction]);
    ExprGenerator eg;
    Opcode opcode;
    do {
        //randomly select opcode 
        opcode = Opcode::FromCode(randomInt(0, 470));
        //opcode = Opcode::I32Const;
        
    } while (!generateStateForOpcode(opcode));
    do {
        generateStateForOpcode(opcode);
        //generate an instrcution
        eg.generateExpr(opcode, State(stateBuf, stateLen));
    } while(!eg.delegate_->pExpr.get());
    ExprList::iterator it = f.exprs.begin();
    // random select location
    size_t p = randomInt((std::size_t)0, f.exprs.size());
    if (p > 0) {
        while (p--) {
            it++;
        }
    }
    //insert an instruction at location
    f.exprs.insert(it, std::move(eg.delegate_->pExpr));
}

void FunctionEditor::eraseInstruction()
{
    Module& module = *pModule;
    //select a function
    Func& f = *(module.funcs[currentFunction]);
    if (f.exprs.size() == 0) {
        return;
    }
    //random select location
    size_t p = randomInt((size_t)0, f.exprs.size() - 1);
    ExprList::iterator it = f.exprs.begin();
    if (p > 0) {
        while (p--) {
            it++;
        }
    }
    //erase an instruction at location
    f.exprs.erase(it);
}

void FunctionEditor::moveInstruction()
{
    Module& module = *pModule;
    Func& f = *(module.funcs[currentFunction]);
    // select one location p
    size_t p = randomInt((size_t)0, f.exprs.size() - 1);
    // select other location p2
    size_t p2 = randomInt((size_t)0, f.exprs.size() - 1);
    //if p=p2 , not move 
    if (f.exprs.size() == 0 || p == p2) {
        return;
    }

    ExprList::iterator it = f.exprs.begin();
    if (p > 0) {
        while (p--) {
            it++;
        }
    }
    ExprList::iterator it2 = f.exprs.begin();
    if (p2 > 0) {
        while (p2--) {
            it2++;
        }
    }
    // move instruction location
    std::unique_ptr<wabt::Expr> itNextValue = f.exprs.extract(it);
    f.exprs.insert(it2, std::move(itNextValue));
}

void FunctionEditor::addFunction() {
    // add a function
    Module& module = *pModule;

    std::vector <uint32_t> validTypes;
    for (uint32_t i = 0; i < module.types.size(); ++i) {
        if (module.types[i]->kind() == TypeEntryKind::Func) {
            validTypes.push_back(i);
        }
    }
    if (validTypes.size() == 0) {
        return;
    }
    // select a function type
    uint32_t funcTypeId = validTypes[randomInt(std::size_t(0), validTypes.size() - 1)];

    FuncModuleField* pfmf = new FuncModuleField;
    Func& f = pfmf->func;

    f.decl.sig = ((FuncType*)(module.types[funcTypeId]))->sig;
    //add function to module 
    module.AppendField(std::unique_ptr<FuncModuleField>(pfmf));
}

void FunctionEditor::eraseFunction() {
    Module& module = *pModule;
    if (module.funcs.size() <= 1) {
        // function number less than 1,return
        return;
    }
#ifdef WASMFUZZER_EXPORT_FUNCTIONS_WITH_NO_ARGUMENTS_ONLY
    if (isExportFunctionsWithNoArgumentsOnly()) {
        return;
    }
#endif // WASMFUZZER_EXPORT_FUNCTIONS_WITH_NO_ARGUMENTS_ONLY
    std::size_t p = randomInt(std::size_t(0), module.funcs.size() - 1);
    // delete module.funcs[p];
    // erase a function
    module.funcs.erase(module.funcs.begin() + p);
}

void FunctionEditor::swapFunction() {
    Module& module = *pModule;
    if (module.funcs.size() == 0) {
        // if function size is 0,return 
        return;
    }
#ifdef WASMFUZZER_EXPORT_FUNCTIONS_WITH_NO_ARGUMENTS_ONLY
    if (isExportFunctionsWithNoArgumentsOnly()) {
        return;
    }
#endif // WASMFUZZER_EXPORT_FUNCTIONS_WITH_NO_ARGUMENTS_ONLY
    //get two function location
    std::size_t p = randomInt(std::size_t(0), module.funcs.size() - 1);
    std::size_t p2 = randomInt(std::size_t(0), module.funcs.size() - 1);
    //swap two functions 
    std::swap(module.funcs[p], module.funcs[p2]);
}


NonFunctionEditor::NonFunctionEditor(Module* pm) : pModule(pm) {
    return;
}

void NonFunctionEditor::addGlobal() {
    //add global item
    Module& module = *pModule;
    //get global field 
    GlobalModuleField* pgmf = new GlobalModuleField;
    Global& global = pgmf->global;
    //global item if mutable 
    global.mutable_ = randomInt(0, 1);
    //select global type 
    global.type = randomInt(-4, -1);
    FunctionEditor fe(nullptr);
    ExprGenerator eg;
    Opcode opcode;
    switch (global.type) {
    case Type::I32:
        opcode = Opcode::I32Const;
        break;
    case Type::I64:
        opcode = Opcode::I64Const;
        break;
    case Type::F32:
        opcode = Opcode::F32Const;
        break;
    case Type::F64:
        opcode = Opcode::F64Const;
        break;
    }
    do {
        fe.generateStateForOpcode(opcode);
        eg.generateExpr(opcode, State(fe.stateBuf, fe.stateLen));
    } while(!eg.delegate_->pExpr.get());
    global.init_expr.push_back(std::move(eg.delegate_->pExpr));
    //add global to module 
    module.AppendField(std::unique_ptr<GlobalModuleField>(pgmf));
}

void NonFunctionEditor::eraseGlobal() {
    Module& module = *pModule;
    if (module.globals.size() == 0) {
        //if no global item , return 
        return;
    }
    // random select global 
    std::size_t p = randomInt(std::size_t(0), module.globals.size() - 1);
    // erase global item
    module.globals.erase(module.globals.begin() + p);
}

void NonFunctionEditor::swapGlobal() {
    Module& module = *pModule;
    if (module.globals.size() == 0) {
        return;
    }
    //random select global items
    std::size_t p = randomInt(std::size_t(0), module.globals.size() - 1);
    std::size_t p2 = randomInt(std::size_t(0), module.globals.size() - 1);
    //swap two global items
    std::swap(module.globals[p], module.globals[p2]);
}

void NonFunctionEditor::addExport() {
    Module& module = *pModule;
    ExportModuleField* pemf = new ExportModuleField;
    Export& ex = pemf->export_;
    //clear the name 
    ex.name.clear();
    uint32_t nameLen = randomInt(1, 10);
    while (nameLen--) {
        //ramdom generate export name 
        ex.name += randomInt<char>('a', 'z');
    }
    for (wabt::Export* pExport : module.exports) {
        if (ex.name == pExport->name) {
            //if name exist ,return
            delete pemf;
            return;
        }
    }
    //random select export kind
    ex.kind = ExternalKind(randomInt(0, 3));
    switch (ex.kind) {
    case ExternalKind::Func:
        if (module.funcs.size() == 0) {
            // if function number is 0,return 
            delete pemf;
            return;
        }
        //random select a function export
        ex.var.set_index(randomInt(std::size_t(0), module.funcs.size() - 1));
#ifdef WASMFUZZER_EXPORT_FUNCTIONS_WITH_NO_ARGUMENTS_ONLY
        if (isExportFunctionsWithNoArgumentsOnly() && module.funcs[ex.var.index()]->GetNumParams() > 0) {
            delete pemf;
            return;
        }
#endif // WASMFUZZER_EXPORT_FUNCTIONS_WITH_NO_ARGUMENTS_ONLY
        break;
    case ExternalKind::Table:
        if (module.tables.size() == 0) {
            // if table number is 0,return
            delete pemf;
            return;
        }
        //random select table export 
        ex.var.set_index(randomInt(std::size_t(0), module.tables.size() - 1));
        break;
    case ExternalKind::Memory:
        if (module.memories.size() == 0) {
            // if memory is 0,return 
            delete pemf;
            return;
        }
        //random select memory export
        ex.var.set_index(randomInt(std::size_t(0), module.memories.size() - 1));
        break;
    case ExternalKind::Global:
        if (module.globals.size() == 0) {
            //if global number is 0,return 
            delete pemf;
            return;
        }
        //random select global export 
        ex.var.set_index(randomInt(std::size_t(0), module.globals.size() - 1));
        break;
    }
    // add export item
    module.AppendField(std::unique_ptr<ExportModuleField>(pemf));
}

void NonFunctionEditor::eraseExport() {
    //erase a export item
    Module& module = *pModule;
    if (module.exports.size() == 0) {
        return;
    }
    //random select export item
    std::size_t p = randomInt(std::size_t(0), module.exports.size() - 1);
    //erase this export item
    module.exports.erase(module.exports.begin() + p);
}

void NonFunctionEditor::swapExport() {
    //swap two export items
    Module& module = *pModule;
    if (module.exports.size() == 0) {
        return;
    }
    // random select two export items
    std::size_t p = randomInt(std::size_t(0), module.exports.size() - 1);
    std::size_t p2 = randomInt(std::size_t(0), module.exports.size() - 1);
    //swap two export items
    std::swap(module.exports[p], module.exports[p2]);
}

void NonFunctionEditor::addImport() {

    return;
}

void NonFunctionEditor::addType() {
    //add type items
    Module& module = *pModule;

    // WebAssembly 1.0 only contains function types.
    FuncType* pft = new FuncType;
    FuncType& ft = *pft;
    //generate param 
    uint32_t n = randomInt(0, 10);
    if (n > 0) {
        while (n--) {
            ft.sig.param_types.push_back(Type(randomInt(-4, -1)));
        }
    }
    //generate result 
    n = randomInt(0, 10);
    if (n > 0) {
        while (n--) {
            ft.sig.result_types.push_back(Type(randomInt(-4, -1)));
        }
    }

    TypeModuleField* ptmf = new TypeModuleField;
    ptmf->type = std::unique_ptr<TypeEntry>(pft);
    // add this type to module 
    module.AppendField(std::unique_ptr<TypeModuleField>(ptmf));
}

void NonFunctionEditor::addTable() {
    // add a table item
    Module& module = *pModule;
    if (module.tables.size() > 0) {
        return;
    }
    TableModuleField* ptmf = new TableModuleField;
    Table& table = ptmf->table;
    uint32_t _2exp = randomInt<uint32_t>(0, 63);
    table.elem_limits.initial = (uint64_t(1) << _2exp);
    table.elem_limits.has_max = randomInt(0, 1);
    if (table.elem_limits.has_max) {
        _2exp = randomInt<uint32_t>(_2exp, 63);
        table.elem_limits.max = (uint64_t(1) << _2exp);
    }
    // 0000027: error: tables may not be shared
    table.elem_limits.is_shared = false;
    // 0000027: error: tables may not be 64-bit
    table.elem_limits.is_64 = false;
    // add this table to module 
    module.AppendField(std::unique_ptr<TableModuleField>(ptmf));
}

void NonFunctionEditor::addMemory() {
    //add a memory item
    Module& module = *pModule;
    if (module.memories.size() > 0) {
        return;
    }
    MemoryModuleField* pmmf = new MemoryModuleField;
    Memory& memory = pmmf->memory;
    uint32_t _2exp = randomInt<uint32_t>(0, 63);
    memory.page_limits.initial = (uint64_t(1) << _2exp);
    memory.page_limits.has_max = randomInt(0, 1);
    if (memory.page_limits.has_max) {
        _2exp = randomInt<uint32_t>(_2exp, 63);
        memory.page_limits.max = (uint64_t(1) << _2exp);
    }
    // 0000026: error: memory may not be shared: threads not allowed
    memory.page_limits.is_shared = false;
    // 0000026: error: memory64 not allowed
    memory.page_limits.is_64 = false;
    //add memory item to module 
    module.AppendField(std::unique_ptr<MemoryModuleField>(pmmf));
}

void NonFunctionEditor::addData() {
    // add a data item
    Module& module = *pModule;
    if (module.memories.size() == 0 || module.data_segments.size() > 0) {
        return;
    }
    DataSegmentModuleField* pdsmf = new DataSegmentModuleField;
    DataSegment& dataSegment = pdsmf->data_segment;
    dataSegment.kind = SegmentKind(randomInt(0, 1));
    if (dataSegment.kind == SegmentKind::Active) {
        for (DataSegment* pds : module.data_segments) {
            if (pds->kind == SegmentKind::Active) {
                dataSegment.kind = SegmentKind::Passive;
                break;
            }
        }
    }
    //random select data size
    uint64_t dataSize = randomInt<uint64_t>(0, std::min<uint64_t>(module.memories[0]->page_limits.initial, 65536));
    dataSegment.data.resize(dataSize);
    for (uint64_t i = 0; i < dataSize; ++i) {
        dataSegment.data.push_back(randomInt<uint8_t>(0x00, 0xFF));
    }
    if (dataSegment.kind == SegmentKind::Active) {
        dataSegment.memory_var.set_index(0);
        uint32_t offset = /*randomInt<uint32_t>(0, module.memories[0]->page_limits.initial - 1 - dataSize)*/0;
        ExprGenerator eg;
        eg.generateExpr(Opcode::I32Const, State((uint8_t*)&offset, sizeof(offset)));
        dataSegment.offset.push_back(std::move(eg.delegate_->pExpr));
    }
    // add data item to module
    module.AppendField(std::unique_ptr<DataSegmentModuleField>(pdsmf));
}

void NonFunctionEditor::setStart() {
    //set start function
    Module& module = *pModule;
    if (module.starts.size() > 0 || module.funcs.size() == 0) {
        //if has start funtion or function number is 0,return 
        return;
    }
    std::vector<uint32_t> validStart;
    for (uint32_t i = 0; i < module.funcs.size(); ++i) {
        if (module.funcs[i]->GetNumParams() == 0 && module.funcs[i]->GetNumResults() == 0) {
            validStart.push_back(i);
        }
    }
    if (validStart.size() == 0) {
        return;
    }
    StartModuleField* psmf = new StartModuleField;
    Var& start = psmf->start;
    start.set_index(validStart[randomInt(std::size_t(0), validStart.size() - 1)]);
    //start.set_index(randomInt(std::size_t(0), module.funcs.size() - 1));
    module.AppendField(std::unique_ptr<StartModuleField>(psmf));
}

void NonFunctionEditor::eraseStart() {
    //erase start function
    Module& module = *pModule;
    if (module.starts.size() == 0) {
        // if start function number is 0 , return 
        return;
    }
    module.starts.erase(module.starts.begin());
}

} 
// end of namespace wasmfuzzer