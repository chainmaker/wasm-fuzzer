# License



wasm-fuzzer is made available under the Apache License, Version 2.0 (Apache-2.0), located in the [LICENSE](./LICENSE) file.

# WasmFuzzer

A fuzzer for vulnerability detection of WebAssembly virtual machines.



Please use `cmake` to build WasmFuzzer.
```
mkdir build
cd build
cmake ..
cmake --build .
```

Fuzz wasmer 
```
wasmfuzzer/build/submodule/afl/wasmfuzzer-exec -i in -o out -m none wasmer @@ -i add 1 2

```

fuzz wasmer example 
```
wasmfuzzer/build/submodule/afl/wasmfuzzer-exec -i ./input -o out -m none ./test/wasmer @@ -i add 1 2

```


Main Parameter Description

-   `-i` dir       &emsp; - input directory with test cases

-   `-o` dir        &emsp;- output directory for fuzzer findings

-  `-m` megs       &emsp;- memory limit for child process

-  `-t` msec       &emsp;- timeout for each run (auto-scaled, 50-1000 ms)

- `@@` &emsp; &emsp;&emsp;&emsp;file holder
