#Copyright (C) Beijing Advanced Innovation Center for Future Blockchain and Privacy Computing (未来区块链与隐私计算高精尖创新中心). All rights reserved.

#SPDX-License-Identifier: Apache-2.0



# 首先执行wasmfuzzer-exec 30s
timeout 30s ./build/submodule/afl/wasmfuzzer-exec -i input -o out -m none ./test/wasmer @@ -i add 1 1

# 执行wasmfuzzer-exec help
./build/submodule/afl/wasmfuzzer-exec -h
# 执行wasmfuzzer-exec helphelp
./build/submodule/afl/wasmfuzzer-exec -hh


## 生成覆盖报告
lcov -c -o afl.info -d ./build/submodule/afl/CMakeFiles/afl-common.dir/src
lcov -c -o afl1.info -d ./build/submodule/afl/CMakeFiles/wasmfuzzer-exec.dir/src
genhtml afl.info -o afl_result
genhtml afl1.info -o afl1_result


cd submodule/wabt
if [ ! -d "./build" ]; then
  mkdir ./build
  cd build 
  cmake ..
  cmake --build .
  cd ..

else
  echo "build directory exit"
fi
# 执行wabt下面的test文件
python3 ./test/run-tests.py

cd ..
cd ..




cd ./submodule/wabt/build
if [ ! -d "./src" ]; then
  mkdir ./src
else
  echo "src directory exit"
fi
cd ..
cd ..
cd ..
cp ./submodule/wabt/src/lexer-keywords.txt ./submodule/wabt/build/src/lexer-keywords.txt
lcov -c -o wabt.info -d ./submodule/wabt/build/CMakeFiles/wabt.dir
genhtml wabt.info -o wabt_result


./build/wasmfuzzer/wasmfuzzer_test
# 生成覆盖报告
lcov -c -o wasmfuzzer.info -d ./build/wasmfuzzer/CMakeFiles/wasmfuzzer_test.dir/
genhtml wasmfuzzer.info -o wasmfuzzer_result
